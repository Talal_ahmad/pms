<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\PermissionController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\DashboardController;
use App\Http\Controllers\Api\PropertyTypeController;
use App\Http\Controllers\Api\PropertyController;
use App\Http\Controllers\Api\RoomTypeController;
use App\Http\Controllers\Api\FloorController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\TableReservationController;
use App\Http\Controllers\Api\MenuCategoryController;
use App\Http\Controllers\Api\MenuServingController;
use App\Http\Controllers\Api\MenuItemController;
use App\Http\Controllers\Api\ReportController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
 */

Broadcast::routes(['middleware' => ['auth:sanctum']]);

Route::middleware(["throttle:10,1"])->group(function () {
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);
});

Route::group(['middleware' => ['auth:sanctum', 'web']], function () {
    Route::post('logout', [AuthController::class, 'logout']);

    // User Routes
    Route::apiResource('users', UserController::class)->middleware('can:edit-user');
    // Role Routes
    Route::apiResource('roles', RoleController::class)->middleware('can:edit-role');
    Route::put('sync-role-permissions/{role}', [RoleController::class, 'syncPermissions'])->can('edit-role');
    Route::get('permissions-by-role/{role}', [RoleController::class, 'permissions'])->can('edit-role');
    // Permission Routes
    Route::apiResource('permissions', PermissionController::class)->only(['index'])->middleware('can:edit-role');
    // Property Routes
    Route::apiResource('property-types', PropertyTypeController::class)->only(['index'])->middleware('can:edit-property');
    Route::apiResource('properties', PropertyController::class);
    Route::apiResource('room-types', RoomTypeController::class)->only(['index'])->middleware('can:edit-property');
    Route::get('properties-for-dashboard', [PropertyController::class, 'propertiesForDashboard'])->can('properties-for-dashboard');

    Route::prefix('properties/{property}')->group(function () {
        Route::get('dashboard', [DashboardController::class, 'index'])->can('view-dashboard');
        Route::apiResource('floors', FloorController::class);
        Route::apiResource('orders', OrderController::class);
        Route::resource('table-reservations', TableReservationController::class);
        Route::put('free-table/{table}', [TableReservationController::class, 'freeTable']);
        Route::apiResource('menu-categories', MenuCategoryController::class);
        Route::apiResource('menu-servings', MenuServingController::class);
        Route::apiResource('menu-items', MenuItemController::class);
        Route::get('menu-items-sales', [MenuItemController::class, 'listingForSales']);
        Route::get('tables-for-making-orders', [TableReservationController::class, 'tablesForMakingOrder']);
        Route::get('kitchen-orders', [OrderController::class, 'getKitchenListing']);
        Route::put('mark-order-ready/{order}', [OrderController::class, 'markReady']);
        Route::post('import-menu-categories/{fromProperty}', [MenuCategoryController::class, 'importFromProperty']);
        Route::post('import-menu-servings/{fromProperty}', [MenuServingController::class, 'importFromProperty']);
        Route::post('import-menu-items/{fromProperty}', [MenuItemController::class, 'importFromProperty']);
        // Report
        Route::get('sale-report', [ReportController::class, 'generateReport']);
    });
});
Route::get('room_floor', [RoomTypeController::class, 'roomfloor']);
