<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            RoleSeeder::class,
            PermissionSeeder::class,
            AssignRoleSeeder::class,
            AssignPermissionSeeder::class,
            PropertyTypeSeeder::class,
            PropertySeeder::class,
            FloorSeeder::class,
            UserSeeder::class,
            RoomTypeSeeder::class,
            AttributeSeeder::class,
            RoomServiceSeeder::class,
            RoomSeeder::class,
            roomConditionSeeder::class,
            HouseKeeperSeeder::class,

        ]);
    }
}
