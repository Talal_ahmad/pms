<?php

namespace Database\Seeders;

use App\Models\RoomService;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoomServiceSeeder extends Seeder
{

    // Run Command : php artisan db:seed --class=RoomServiceSeeder


    // sanitary (for bathroom and room cleanliness)
    // electronics (bulb, ac, heater, fan, tv, internet, etc)
    // construction (repairing breakage)
    // furniture (bed, table, chair)
    // furnish (curtains, carpets, etc)
    // paint (repaint required or not))
    
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // will not add created_at and updated_at columns' values in table (they will be null)
        // RoomService::insert([
        //     ['name' => 'sanitary'],
        //     ['name' => 'electronics'],
        // ]);
        
        $room_services = ['sanitary', 'electronics', 'construction', 'furniture', 'paint', 'furnish','condition'];
        foreach ($room_services as $room_service) {
            RoomService::create([
                'name' => $room_service
            ]);
        }
    }
}
