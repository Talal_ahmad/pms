<?php

namespace Database\Seeders;

use App\Models\Floor;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FloorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        Floor::Create([
            'property_id' => 2,
            'name' => 'Demo Hotel Floor 1',
        ]);

        // $floors = range(1, 10);

        // foreach ($floors as $key => $value) {
        //     Floor::Create([
        //         'property_id' => 2,
        //         'name' => 'Demo Hotel Floor ' . $value,
        //     ]);
        // }  
        
    }
}
