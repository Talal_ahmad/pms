<?php

namespace Database\Seeders;

use App\Models\RoomType;
use Illuminate\Database\Seeder;

class RoomTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $roomTypes = [
            'Single',
            'Double',
            'Suite',
            'Deluxe Suite',
            'Executive Room',
            'Family Suite',
        ];

        foreach ($roomTypes as $type) {
            RoomType::updateOrCreate([
                'name' => $type,
            ]);
        }
    }
}
