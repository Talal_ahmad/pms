<?php

namespace Database\Seeders;

use App\Models\User;
use Hash;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $users = [
            ['name' => 'Demo Super Admin', 'email' => 'admin@pms.com', 'password' => '123456789', 'property_id' => ['1', '2', '3'], 'status' => 1],
            ['name' => 'Demo House Keeper', 'email' => 'housekeeper@pms.com', 'password' => '123456789', 'property_id' => ['2'], 'status' => 1],
        ];

        foreach ($users as $user) {
            User::create([
                'name' => $user['name'],
                'email' => $user['email'],
                'password' => Hash::make($user['password']),
                'property_id' => $user['property_id'],
                'status' => $user['status'],
            ]);
        }

        // User::create([
        //     'name' => 'Super Admin',
        //     'email' => 'admin@pms.com',
        //     'password' => Hash::make('123456789'),
        //     'property_id' => 2,
        // ]);
    }
}
