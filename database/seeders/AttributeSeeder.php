<?php

namespace Database\Seeders;

use App\Models\Attribute;
use Illuminate\Database\Seeder;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $attributes = [
            [
                'name' => 'Air Conditioning',
                'description' => 'Room has air conditioning',
            ],
            [
                'name' => 'Wi-Fi',
                'description' => 'Room has Wi-Fi',
            ],
            [
                'name' => 'TV',
                'description' => 'Room has a television',
            ],
            [
                'name' => 'Mini Fridge',
                'description' => 'Room has a mini fridge',
            ],
            [
                'name' => 'Ocean View',
                'description' => 'Room has an ocean view',
            ],
        ];

        foreach ($attributes as $attribute) {
            Attribute::updateOrCreate([
                'name' => $attribute['name'],
                'description' => $attribute['description'],
            ]);
        }
    }
}
