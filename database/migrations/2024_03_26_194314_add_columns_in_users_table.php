<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            if (!Schema::hasColumn('users', 'property_id')) {
                $table->string('property_id', 255)->nullable()->after('id');
            }
            if (!Schema::hasColumn('users', 'train')) {
                $table->string('train')->nullable()->after('password');
            }
            if (!Schema::hasColumn('users', 'phone_number')) {
                $table->string('phone_number', 20)->nullable()->after('train');
            }
            if (!Schema::hasColumn('users', 'cnic')) {
                $table->string('cnic', 15)->nullable()->after('phone_number');
            }
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('property_id');
            $table->dropColumn('train');
            $table->dropColumn('phone_number');
            $table->dropColumn('cnic');
        });
    }
};
