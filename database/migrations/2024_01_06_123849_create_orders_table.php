<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('property_id')->constrained();
            $table->foreignId('reservation_id')->nullable()->constrained();
            $table->foreignId('customer_id')->nullable()->constrained();
            $table->string('order_number');
            $table->double('gst', 8, 2)->default(0);
            $table->string('payment_method');
            $table->double('discount', 8, 2)->default(0);
            $table->double('service_charges', 8, 2)->default(0);
            $table->enum('type', ['dine-in', 'delivery', 'pick-up'])->default('dine-in');
            $table->double('price', 8, 2);
            $table->enum('status', ['pending', 'processing', 'completed'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
