<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('booking_rooms', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable(); // Make user_id nullable
            $table->unsignedBigInteger('room_id');
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('customer_type_id')->nullable();
            $table->unsignedBigInteger('reference_id')->nullable();
            $table->unsignedBigInteger('company_reference_id')->nullable();
            $table->unsignedInteger('female_count')->default(0);
            $table->unsignedInteger('male_count')->default(0);
            $table->unsignedInteger('child_count')->default(0);
            $table->unsignedInteger('infant_count')->default(0);
            $table->unsignedInteger('stay_length');
            $table->date('fromDateTime');
            $table->date('toDateTime');
            $table->dateTime('arrivalDateTime')->nullable(); // Make arrivalDateTime nullable
            $table->dateTime('departureDateTime')->nullable();
            $table->string('description')->nullable();
            $table->tinyInteger('cancel')->default(0); // Added cancel field with default value 0
            $table->json('complementary_food');
            $table->unsignedBigInteger('shift_room_id')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->nullable(); // Make user_id nullable
            // $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('customer_type_id')->references('id')->on('customer_types')->nullable();
            $table->foreign('reference_id')->references('id')->on('references')->nullable();
            $table->foreign('company_reference_id')->references('id')->on('company_references')->nullable();
            $table->foreign('room_id')->references('id')->on('rooms');
        });
    }


    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('booking_rooms');
    }
};
