<?php

namespace App\Http\Controllers;

use App\Http\Requests\VehicleRequest;
use DataTables;
use DB;
use App\Models\Vehicle;
use App\Models\Property;
use App\Models\VehicleType;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Property $property)
    {
        $vehicles = $property->vehicles;
        $vehiclesTypes = VehicleType::where('property_id', $property->id)->get();

        if ($request->ajax()) {
            return DataTables::of($vehicles)->addIndexColumn()->make(true);
        }
        return view('admin.vehicle.index', compact('vehiclesTypes'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(VehicleRequest $request)
    {
        try {
            $propertyId = $request->input('property_id');

            // Find the property by its ID
            $property = Property::findOrFail($propertyId);

            // Merge the property_id into the request data
            $requestData = $request->validated();
            $requestData['property_id'] = $property->id;

            // Use transaction to ensure data integrity
            DB::transaction(function () use ($requestData) {
                Vehicle::create($requestData);
            });

            // Return success response
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            // Handle exceptions
            if ($e instanceof ValidationException) {
                // Return validation error response
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                // Return generic error response
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Vehicle $vehicle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Property $property, Vehicle $vehicle)
    {
        return response()->json($vehicle);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(VehicleRequest $request, Property $property, Vehicle $vehicle)
    {
        try {
            $requestData = $request->merge(['property_id' => $property->id]);

            DB::transaction(function () use ($requestData, $vehicle) {
                $vehicle->update($requestData->all());
            });
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Property $property, Vehicle $vehicle)
    {
        try {
            $vehicle->delete();

            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
