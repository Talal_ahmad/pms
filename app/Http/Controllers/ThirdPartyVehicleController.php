<?php

namespace App\Http\Controllers;

use App\Models\Property;
use App\Models\ThirdPartyVehicle;
use DataTables;
use DB;
use App\Http\Requests\ThirdPartyVehicleRequest;
use Illuminate\Http\Request;

class ThirdPartyVehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Property $property)
    {
        $ThirdPartyVehicle = ThirdPartyVehicle::all();
        // dd($ThirdPartyVehicle);
        if ($request->ajax()) {
            return DataTables::of($ThirdPartyVehicle)->addIndexColumn()->make(true);
        }
        return view('admin.ThirdPartyVehicle.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ThirdPartyVehicleRequest $request)
    {
        try {
            DB::transaction(function () use ($request) {
                ThirdPartyVehicle::create($request->validated());
            });
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(ThirdPartyVehicle $thirdPartyVehicle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Property $property, ThirdPartyVehicle $thirdPartyVehicle)
    {
        return response()->json($thirdPartyVehicle);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ThirdPartyVehicleRequest $request, Property $property, ThirdPartyVehicle $thirdPartyVehicle)
    {
        try {

            DB::transaction(function () use ($request, $thirdPartyVehicle) {
                $thirdPartyVehicle->update($request->validated());
            });

            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Property $property, ThirdPartyVehicle $thirdPartyVehicle)
    {
        try {
            $thirdPartyVehicle->delete();

            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
