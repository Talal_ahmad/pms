<?php

namespace App\Http\Controllers;

use App\Models\CompanyReference;
use App\Models\Property;
use App\Http\Requests\CompanyReferenceRequest;
use DataTables;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;


class CompanyReferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Property $property)
    {
        $Company_reference = CompanyReference::all();
        // dd($Company_reference);
        if ($request->ajax()) {
            return DataTables::of($Company_reference)->addIndexColumn()->make(true);
        }
        return view('admin.companyReference.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CompanyReferenceRequest $request)
    {
        try {
            DB::transaction(function () use ($request) {
                CompanyReference::create($request->validated());
            });
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Property $property, CompanyReference $companyReference)
    {
        return response()->json($companyReference);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CompanyReferenceRequest $request, Property $property, CompanyReference $companyReference)
    {
        try {

            DB::transaction(function () use ($request, $companyReference) {
                $companyReference->update($request->validated());
            });

            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Property $property, CompanyReference $companyReference)
    {
        try {
            $companyReference->delete();

            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
