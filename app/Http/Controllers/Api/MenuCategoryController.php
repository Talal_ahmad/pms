<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\MenuCategory;
use App\Models\Property;
use Illuminate\Http\Request;
use App\Http\Resources\MenuCategoryCollection;
use App\Http\Resources\MenuCategoryResource;
use App\Http\Requests\MenuCategoryRequest;

class MenuCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Property $property)
    {
        $menuCategories = $property->menuCategories;
        return new MenuCategoryCollection($menuCategories);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MenuCategoryRequest $request, Property $property)
    {
        $request->merge(['property_id' => $property->id]);
        $menuCategory = MenuCategory::create($request->all());
        return new MenuCategoryResource($menuCategory);
    }

    /**
     * Display the specified resource.
     */
    public function show(Property $property, MenuCategory $menuCategory)
    {
        if ($menuCategory->property_id != $property->id) {
            abort(401);
        }

        return new MenuCategoryResource($menuCategory);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MenuCategoryRequest $request, Property $property, MenuCategory $menuCategory)
    {
        if ($menuCategory->property_id != $property->id) {
            abort(401);
        }

        $request->merge(['property_id' => $property->id]);
        $menuCategory->update($request->all());
        return new MenuCategoryResource($menuCategory);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Property $property, MenuCategory $menuCategory)
    {
        if ($menuCategory->property_id != $property->id) {
            abort(401);
        }
        
        $menuCategory->delete();
        return response()->json([
            'message' => 'Menu Category Deleted Successfully!',
        ]);
    }

    public function importFromProperty(Property $property, Property $fromProperty){
        foreach ($fromProperty->menuCategories as $menuCategory) {
            $property->menuCategories()->updateOrCreate([
                'name' => $menuCategory->name, 
            ],[
                'name' => $menuCategory->name, 
            ]);
        }
        return response()->json([
            'message' => 'Menu Categories Imported Successfully!',
        ]);
    }
}
