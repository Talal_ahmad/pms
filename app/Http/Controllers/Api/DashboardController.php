<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Property;
use App\Models\Order;

class DashboardController extends Controller
{
    public function index(Property $property)
    {
        $data['cards']['total_customers'] = Customer::where('property_id', $property->id)->count();
        $data['cards']['total_orders'] = Order::where('property_id', $property->id)->count();
        return response()->json([
            'data' => $data,
        ]);
    }
}
