<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\TableReservationRequest;
use App\Http\Resources\ReservationCollection;
use App\Http\Resources\ReservationResource;
use App\Http\Resources\CreateReservationCollection;
use App\Models\Reservation;
use App\Models\Property;
use App\Models\Customer;
use App\Models\Table;
use DB;

class TableReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Property $property)
    {
        $reservations = Reservation::whereHas('customer', function ($query) use ($property){
            $query->where('property_id', $property->id);
        })->with(['table.floor', 'customer'])->get();
        return new ReservationCollection($reservations);
    }

    /**
     * Send Data To Create the new Resource.
     */
    public function create(Property $property)
    {
        $tables = Table::whereHas('floor', function ($query) use($property) {
            $query->where('property_id', $property->id);
        })->with('reservations.customer')->get();
        return new CreateReservationCollection($tables);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TableReservationRequest $request, Property $property)
    {
        $table = Table::find($request->table_id);
        if($table->floor->property_id != $property->id){
            abort(401);
        }

        DB::transaction(function () use ($request, $property, $table) {
            $customerData = [
                'property_id' => $property->id,
                'name' => $request->name,
            ];
            $customer = Customer::create($customerData);
            $table->reservations()->create([
                'customer_id' => $customer->id,
                'start_date_time' => $request->fromDateTime,
                'end_date_time' => $request->toDateTime,
            ]);
        });
        return response()->json([
            'message' => 'Reservation Created Successfully!',
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Property $property, Reservation $tableReservation)
    {
        if ($tableReservation->customer->property_id != $property->id) {
            abort(401);
        }

        return new ReservationResource($tableReservation->load('customer'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TableReservationRequest $request, Property $property, Reservation $tableReservation)
    {
        if ($tableReservation->customer->property_id != $property->id) {
            abort(401);
        }

        DB::transaction(function () use ($request, $property, $tableReservation) {
            $customerData = [
                'property_id' => $property->id,
                'type' => $request->customer_type,
                'name' => $request->name,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
            ];
            $customer = Customer::find($request->customer_id);
            $customer->update($customerData);
            $tableReservation->update([
                'type' => $request->type,
                'customer_id' => $customer->id,
                'number_of_persons' => $request->number_of_persons,
                'start_date_time' => $request->fromDateTime,
                'end_date_time' => $request->toDateTime,
                'remarks' => $request->remarks,
            ]);
        });
        return response()->json([
            'message' => 'Reservation Updated Successfully!',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Property $property, Reservation $tableReservation)
    {
        if ($tableReservation->customer->property_id != $property->id) {
            abort(401);
        }
        
        $tableReservation->delete();
        return response()->json([
            'message' => 'Reservation Deleted Successfully!',
        ]);
    }

    public function freeTable(Property $property, Table $table)
    {
        if ($table->floor->property_id != $property->id) {
            abort(401);
        }

        $table->update([
            'status' => 'available',
        ]);
        return response()->json([
            'message' => 'Table Status Updated Successfully!',
        ]);
    }

    public function tablesForMakingOrder(Property $property){
        $tables = Table::whereHas('floor', function ($query) use ($property) {
            $query->where('property_id', $property->id);
        })->get(['id', 'name']);
        return response()->json([
            'data' => $tables,
        ]);
    }
}
