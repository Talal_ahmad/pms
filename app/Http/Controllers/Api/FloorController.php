<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FloorRequest;
use App\Http\Resources\FloorCollection;
use App\Http\Resources\FloorResource;
use App\Models\Floor;
use App\Models\Property;
use DB;

class FloorController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Property $property)
    {
        $floors = $property->floors;
        return new FloorCollection($floors);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(FloorRequest $request, Property $property)
    {
        DB::transaction(function () use ($request, $property) {
            foreach ($request->floors as $floor) {
                $floorObj = $property->floors()->create($floor);
                foreach ($floor['rooms'] as $room) {
                    $floorObj->rooms()->create($room);
                }
                foreach ($floor['tables'] as $table) {
                    $floorObj->tables()->create($table);
                }
            }
        });
        return response()->json([
            'message' => 'Floors Created Successfully!',
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Property $property, Floor $floor)
    {
        if ($floor->property_id != $property->id) {
            abort(401);
        }

        return new FloorResource($floor);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(FloorRequest $request, Property $property, Floor $floor)
    {
        if ($floor->property_id != $property->id) {
            abort(401);
        }

        DB::transaction(function () use ($request, $floor) {
            $floor->update($request->validated());

            $roomIds = collect($request->rooms)->pluck('id')->filter();
            $floor->rooms()->whereNotIn('id', $roomIds)->delete();

            foreach ($request->rooms as $room) {
                $floor->rooms()->updateOrCreate(
                    ['id' => $room['id'] ?? 0],
                    $room
                );
            }

            $tableIds = collect($request->tables)->pluck('id')->filter();
            $floor->tables()->whereNotIn('id', $tableIds)->delete();
            foreach ($request->tables as $table) {
                $floor->tables()->updateOrCreate(
                    ['id' => $table['id'] ?? 0],
                    $table
                );
            }
        });
        return response()->json([
            'message' => 'Floor Updated Successfully!',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Property $property, Floor $floor)
    {
        if ($floor->property_id != $property->id) {
            abort(401);
        }

        $floor->delete();
        return response()->json([
            'message' => 'Floor Deleted Successfully!',
        ]);
    }
}
