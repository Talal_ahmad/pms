<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MenuItemCollection;
use App\Http\Resources\MenuItemResource;
use App\Models\Property;
use DB;
use App\Models\MenuItem;
use Illuminate\Http\Request;
use App\Http\Requests\MenuItemRequest;
use Illuminate\Support\Facades\Storage;

class MenuItemController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Property $property)
    {
        $menuItems = MenuItem::whereHas('menuCategory', function ($query) use ($property){
            $query->where('property_id', $property->id);
        })->with('menuCategory')->get();
        return new MenuItemCollection($menuItems);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MenuItemRequest $request, Property $property)
    {
        DB::transaction(function () use ($request) {
            $data = $request->input('image');
            if (!empty($data)) {
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);
                $file_path = 'images';
                $path = public_path() . '/' . $file_path;
                $filename = '/' . time() . '_' . rand(000, 999) . '.' . 'png';
    
                $imageName = $file_path . $filename;
    
                file_put_contents($imageName, $data);
            }

            $menuItem = MenuItem::create([
                'menu_category_id' => $request->menu_category_id,
                'name' => $request->name,
                'image' => ($imageName) ? $imageName : NULL, // Set image with default value of null
            ]);
            foreach ($request->servings as $serving) {
                $menuItem->menuItemDetails()->create($serving);
            }
        });
        return response()->json([
            'message' => 'Floors Created Successfully!',
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Property $property, MenuItem $menuItem)
    {
        if ($menuItem->menuCategory->property_id != $property->id) {
            abort(401);
        }

        return new MenuItemResource($menuItem->load('menuItemDetails'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MenuItemRequest $request, Property $property, MenuItem $menuItem)
    {
        if ($menuItem->menuCategory->property_id != $property->id) {
            abort(401);
        }

        DB::transaction(function () use ($request, $menuItem) {

            $data = $request->input('image');
            if(!empty($data)){
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);
                $file_path = 'images';
                $path = public_path() . '/' . $file_path;
                $filename = '/' . time() . '_' . rand(000, 999) . '.' . 'png';
    
                $imageName = $file_path . $filename;
                if ($menuItem->image) {
                    if (file_exists(public_path()."/".$menuItem->image)) {
                        unlink(public_path() . '/' . $menuItem->image);
                    }
                }
    
                file_put_contents($imageName, $data);
            }

            $menuItem->update([
                'menu_category_id' => $request->menu_category_id,
                'name' => $request->name,
                'image' => ($imageName) ? $imageName : $menuItem->image,
            ]);
            $servingsIds = collect($request->servings)->pluck('id')->filter();
            $menuItem->menuItemDetails()->whereNotIn('id', $servingsIds)->delete();

            foreach ($request->servings as $serving) {
                $menuItem->menuItemDetails()->updateOrCreate(
                    ['id' => $serving['id'] ?? 0],
                    $serving
                );
            }
        });
        return response()->json([
            'message' => 'Item Updated Successfully!',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Property $property, MenuItem $menuItem)
    {
        if ($menuItem->menuCategory->property_id != $property->id) {
            abort(401);
        }
        
        $menuItem->menuItemDetails()->delete();
        $menuItem->delete();
        return response()->json([
            'message' => 'Menu Item Deleted Successfully!',
        ]);
    }

    public function listingForSales(Property $property)
    {
        $menuItems = MenuItem::whereHas('menuCategory', function ($query) use ($property){
            $query->where('property_id', $property->id);
        })->with(['menuItemDetails.menuServing', 'menuItemDetails.menuServingQuantity'])->get();
        return new MenuItemCollection($menuItems);
    }

    public function importFromProperty(Property $property, Property $fromProperty){
        $items = MenuItem::whereHas('menuCategory', function ($query) use ($fromProperty){
            $query->where('property_id', $fromProperty->id);
        })->with(['menuCategory', 'menuItemDetails.menuServing', 'menuItemDetails.menuServingQuantity'])->get();
        DB::transaction(function () use ($property, $items) {
            foreach ($items as $item) {
                $category = $property->menuCategories()->where('name', $item->menuCategory->name)->first();
                $menuItem = MenuItem::updateOrCreate([
                    'menu_category_id' => $category->id,
                    'name' => $item->name,
                ],
                [
                    'menu_category_id' => $category->id,
                    'name' => $item->name,
                    'image' => $item->name,
                ]);
                $menuItem->menuItemDetails()->delete();
                foreach ($item->menuItemDetails as $menuItemDetail) {
                    $serving = $property->menuServings()->where('name', optional($menuItemDetail->menuServing)->name)->first();
                    $servingQuantity = $property->menuServings()->where('name', optional($menuItemDetail->menuServingQuantity)->name)->first();
                    $menuItem->menuItemDetails()->create([
                        'menu_serving_id' => !empty($serving) ? $serving->id : null,
                        'menu_serving_quantity_id' => !empty($servingQuantity) ? $servingQuantity->id : null,
                        'price' => $menuItemDetail->price,
                    ]);
                }
            }
        });
        return response()->json([
            'message' => 'Menu Items Imported Successfully!',
        ]);
    }
}
