<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\OrderDetail;
use Carbon\Carbon;

class ReportController extends Controller
{
    public function generateReport(Request $request)
    {
        $request->validate([
            'fromDate' => 'date_format:Y-m-d',
            'toDate' => 'date_format:Y-m-d',
        ]);

        if ($request->has('fromDate') && $request->has('toDate')) {
            // Retrieve the fromDate and toDate from the request, or set default values
            $fromDate = $request->input('fromDate', Carbon::now()->subWeek()->startOfDay()->format('Y-m-d'));
            $toDate = $request->input('toDate', Carbon::now()->endOfDay()->format('Y-m-d'));

            $reportData = OrderDetail::join('orders', 'orders.id', '=', 'order_details.order_id')
                ->join('menu_items', 'menu_items.id', '=', 'order_details.menu_item_id')
                ->leftjoin('menu_servings', 'menu_servings.id', '=', 'order_details.menu_serving_id')
                ->wheredate('order_details.created_at','>=',$fromDate)
                ->wheredate('order_details.created_at','<=',$toDate)
                ->select(
                    'order_details.*',
                    'menu_items.name as itemName',
                    'menu_servings.name as servingName',
                )
                ->get();
            return response()->json(['Result' => $reportData]);
        } else {
            return response()->json(['Error' => 'Both From Date and To Date are required.'], 400);
        }

    }
}
