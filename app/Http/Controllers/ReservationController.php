<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Rules\CheckTablePreReservationTimeOverlap;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\TableReservationRequest;
use App\Http\Resources\ReservationCollection;
use App\Http\Resources\ReservationResource;
use App\Http\Resources\CreateReservationCollection;
use App\Models\Reservation;
use App\Models\Property;
use App\Models\Customer;
use App\Models\Table;
use DB;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\DataTables;


class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Property $property)
    {
        $reservations = Reservation::whereHas('customer', function ($query) use ($property){
            $query->where('property_id', $property->id);
        })->with(['table.floor', 'customer']);

        if ($request->ajax()) {
            return DataTables::of($reservations)->addIndexColumn()->make(true);
        }

        $tables = Table::whereHas('floor', function ($query) use($property) {
            $query->where('property_id', $property->id);
        })->with('reservations.customer')->get();

        $rooms = Room::whereHas('floor', function ($query) use($property) {
            $query->where('property_id', $property->id);
        })->with('reservations.customer')->get();

        return view('admin.reservations.index', compact(["tables", "rooms"]))
               ->with(["propertyType" => $property->propertyType->name]);
    }

    /**
     * Send Data To Create the new Resource.
     */
    public function create(Property $property)
    {
        $tables = Table::whereHas('floor', function ($query) use($property) {
            $query->where('property_id', $property->id);
        })->with('reservations.customer')->get();
        return new CreateReservationCollection($tables);
    }

    /**
     * Store a newly created resource in storage.
     */
    // public function store(TableReservationRequest $request, Property $property)
    public function store(Request $request, Property $property)
    {
        try{

            $this->validate($request , [
                'name' => 'required',
                'fromDateTime' => ['required', new CheckTablePreReservationTimeOverlap($request->input('table_id'), $request->input('fromDateTime'), $request->input('toDateTime'))],
            ]);

            $table = $room = null;

            if($property->propertyType->name === "Restaurant") {

                $table = Table::find($request->table_id);
                if($table->floor->property_id != $property->id){
                    abort(401);
                }
            }
            elseif($property->propertyType->name === "Hotel") {

                $room = Room::find($request->room_id);
                if($room->floor->property_id != $property->id){
                    abort(401);
                }
            }
            
            // $table = Table::find($request->table_id);
            // if($table->floor->property_id != $property->id){
            //     abort(401);
            // }


            DB::transaction(function () use ($request, $property, $table , $room) {
                $customerData = [
                    'property_id' => $property->id,
                    'name' => $request->name,
                ];
                $customer = Customer::create($customerData);
                // $table->reservations()->create([
                //     'customer_id' => $customer->id,
                //     'start_date_time' => $request->fromDateTime,
                //     'end_date_time' => $request->toDateTime,
                // ]);
                if($property->propertyType->name === "Restaurant") {

                    $table->reservations()->create([
                        'customer_id' => $customer->id,
                        'start_date_time' => $request->fromDateTime,
                        'end_date_time' => $request->toDateTime,
                    ]);
                }
                elseif($property->propertyType->name === "Hotel") {
    
                    $room->reservations()->create([
                        'customer_id' => $customer->id,
                        'start_date_time' => $request->fromDateTime,
                        'end_date_time' => $request->toDateTime,
                    ]);
                }
            });
            return response()->json([
                'message' => 'Reservation Created Successfully!',
            ]);

        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    // public function show(Property $property, Reservation $tableReservation)
    public function show(Property $property, $reservationId)
    {
        $tableReservation = Reservation::find($reservationId);
        // echo $tableReservation;
        // return;

        if ($tableReservation->customer->property_id != $property->id) {
            abort(401);
        }

        return new ReservationResource($tableReservation->load('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    // public function edit(Property $property, Reservation $tableReservation)
    public function edit(Property $property, $reservationId)
    {
        $tableReservation = Reservation::find($reservationId);
        // echo $tableReservation;
        // return;

        if ($tableReservation->customer->property_id != $property->id) {
            abort(401);
        }

        $tables = Table::whereHas('floor', function ($query) use($property) {
            $query->where('property_id', $property->id);
        })->with('reservations.customer')->get();

        return response()->json([
            "tables" => $tables,
            "reservation" => $tableReservation->load('customer')
        ]);
        
        // return new CreateReservationCollection($tables);
        // return new ReservationResource($tableReservation->load('customer'));
    }

    /**
     * Update the specified resource in storage.
     */
    // public function update(Request $request, Property $property, Reservation $tableReservation)
    public function update(Request $request, Property $property, $reservationId)
    {
        try{

            $this->validate($request , [
                'name' => 'required',
                // 'fromDateTime' => ['required', new CheckTablePreReservationTimeOverlap($request->table_reservation->table->id, $request->input('fromDateTime'), $request->input('toDateTime'), $request->table_reservation->table->id)]
                'fromDateTime' => 'required'
            ]);

            $tableReservation = Reservation::find($reservationId);
            
            if ($tableReservation->customer->property_id != $property->id) {
                abort(401);
            }

            DB::transaction(function () use ($request, $property, $tableReservation) {
                $customerData = [
                    'property_id' => $property->id,
                    'type' => $request->customer_type,
                    'name' => $request->name,
                    'email' => $request->email,
                    'phone_number' => $request->phone_number,
                ];
                $customer = Customer::find($request->customer_id);
                $customer->update($customerData);
                $tableReservation->update([
                    'type' => $request->type,
                    'customer_id' => $customer->id,
                    'number_of_persons' => $request->number_of_persons,
                    'start_date_time' => $request->fromDateTime,
                    'end_date_time' => $request->toDateTime,
                    'remarks' => $request->remarks,
                ]);
            });
            return response()->json([
                'message' => 'Reservation Updated Successfully!',
            ]);

        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    // public function destroy(Property $property, Reservation $tableReservation)
    public function destroy(Property $property, $reservationId)
    {
        $tableReservation = Reservation::find($reservationId);
        
        if ($tableReservation->customer->property_id != $property->id) {
            abort(401);
        }
        
        $tableReservation->delete();
        return response()->json([
            'message' => 'Reservation Deleted Successfully!',
        ]);
    }

    public function freeTable(Property $property, Table $table)
    {
        if ($table->floor->property_id != $property->id) {
            abort(401);
        }

        $table->update([
            'status' => 'available',
        ]);
        return response()->json([
            'message' => 'Table Status Updated Successfully!',
        ]);
    }

    public function tablesForMakingOrder(Property $property){
        $tables = Table::whereHas('floor', function ($query) use ($property) {
            $query->where('property_id', $property->id);
        })->get(['id', 'name']);
        return response()->json([
            'data' => $tables,
        ]);
    }

    // public function removeSecondsFromDateTime($value)
    // {
    //     // Parse the incoming date and time
    //     // $dateTime = new DateTime($value);
    //     $dateTime = Carbon::parse($value);

    //     // Format the date and time without seconds
    //     return $dateTime->format('Y-m-d H:i');
    // }
    
}
