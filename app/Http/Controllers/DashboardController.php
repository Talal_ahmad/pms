<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Property;
use Illuminate\Support\Facades\Auth;


class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();
        $propertyIds = $user->property_id;
        $Property = Property::join('property_types', 'property_types.id', '=', 'properties.property_type_id')
            ->whereIn('properties.id', $propertyIds)
            ->select('properties.*', 'property_types.name as type_name')
            ->get();
        return view('admin.index',  compact('Property'));
    }
}
