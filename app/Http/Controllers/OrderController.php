<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Property;
use App\Models\Customer;
use App\Models\Table;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\OrderResource;
use App\Http\Requests\OrderRequest;
use Illuminate\Http\Request;
use App\Models\User;
use App\Events\OrderPlaced;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Property $property)
    {
        if ($request->dateFilter != 'Invalid Date') {
            $date = $request->dateFilter;
        } else {
            $date = Carbon::today()->toDateString();
        }
        $orders = $property->orders()->with(['customer'])->get();
        // dd($orders);
        if ($request->ajax()) {
            return DataTables::of($orders)->addIndexColumn()->make(true);
        }

        return view('admin.sales.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Property $property)
    {
        return view('admin.sales.create');
    }

    /**
     * Show the form for editing a resource.
     */
    public function edit(Property $property, $order)
    {
        return view('admin.sales.edit', compact('order'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Property $property, Order $order)
    {
        if ($order->property_id != $property->id) {
            abort(401);
        }

        return new OrderResource($order->load(['customer', 'orderDetails.menuItem', 'orderDetails.menuServing', 'orderDetails.menuServingQuantity']));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Property $property, Order $order)
    {
        if ($order->property_id != $property->id) {
            abort(401);
        }

        $order->orderDetails->map->delete();
        $order->delete();
        return response()->json([
            'message' => 'Order Deleted Successfully!',
        ]);
    }

    private function findTableCurrentReservation($table)
    {
        $table = Table::find($table);
        return $table->currentReservation;
    }

    public function getKitchenListing(Property $property)
    {

        $orders = $property->orders()
            ->with(['orderDetails.menuItem', 'orderDetails.menuServing', 'orderDetails.menuServingQuantity'])
            // ->where('status', 'pending')
            ->orderBy('created_at')
            ->get();
        // dd($orders);
        return view('admin.kitchen.index', compact('orders'));
    }

    public function markReady(Property $property, Order $order)
    {
        try {
            $order->update([
                'status' => 'completed',
            ]);

            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }
    public function tablesForMakingOrder(Property $property)
    {
        $tables = Table::whereHas('floor', function ($query) use ($property) {
            $query->where('property_id', $property->id);
        })->get(['id', 'name']);

        return view('admin.table.index');
    }
}
