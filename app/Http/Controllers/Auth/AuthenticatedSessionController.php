<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use App\Models\User;


class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     */
    public function create(): View
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     */
    public function store(LoginRequest $request): RedirectResponse
    {
        // $request->authenticate();

        // $request->session()->regenerate();

        // return redirect()->intended(RouteServiceProvider::HOME);

        // Authenticate the user
        $request->authenticate();

        // Regenerate the session
        $request->session()->regenerate();

        // Get the credentials from the request
        $credentials = $request->only('email', 'password');

        // Check if the authenticated user is active
        if (auth()->user()->status === 1) {
            // Attempt to authenticate with the credentials
            if (Auth::attempt($credentials)) {
                // Find the user
                $user = User::find(auth()->user()->id);

                // Update the user's online status
                $user->status = 1;
                $user->update();

                // Check the user's roles and redirect accordingly
                if (auth()->user()->hasRole('Admin') || auth()->user()->hasRole('Super Admin')) {
                    // Redirect to home page if the user is an admin or super admin
                    return redirect()->intended(RouteServiceProvider::HOME);
                } else {
                    // Redirect to users page if the user is not an admin or super admin
                    return redirect('/');
                }
            }
        } else {
            // If the user is inactive, log them out and return an error message
            Auth::logout();
            return back()->with('error', 'Your account is inactive. Please contact support.');
        }

        // Add a return statement at the end to handle all other cases
        return redirect()->route('login')->with('error', 'Unable to authenticate. Please try again.');
    }

    /**
     * Destroy an authenticated session.
     */
    public function destroy(Request $request): RedirectResponse
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
