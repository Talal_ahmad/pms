<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoomConditionRequest;
use App\Models\BookingRoom;
use App\Models\Floor;
use App\Models\HouseKeeperRoom;
use App\Models\Property;
use App\Models\Room;
use App\Models\RoomCondition;
use App\Models\RoomService;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use DB;

class RoomConditionController extends Controller
{

    // "house keeper" credentials:
    // email = housekeeper@pms.com
    // password = 123456789



    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Property $property)
    {
        // $house_keeper = HouseKeeper::where('user_id', auth()->id())->first();
        $house_keeper = HouseKeeperRoom::with(['user:id,name', 'floor:id,name'])->where('user_id', auth()->id())->first();
        // dd($house_keeper);

        // $rooms = Floor::where('property_id', $property->id)->first()->rooms;

        // $rooms = Room::where('floor_id', $house_keeper->floor->id)->get();
        $rooms = [];
        if(!empty($house_keeper)){
            $rooms = RoomCondition::join('rooms', 'room_conditions.room_id', 'rooms.id')
                ->join('room_services', 'room_conditions.room_service_id', 'room_services.id')
                ->where('rooms.floor_id', $house_keeper->floor->id)
                ->where('room_services.id', 7)
                ->select(
                    'room_services.name as service_name',
                    'room_conditions.*',
                    'rooms.name',
                    'rooms.id',
                )
                ->get();
        }

        // Output the results
        // dd($rooms);

        $room_services = RoomService::all();

        // dd($house_keeper, $rooms, $room_services);
        // dd($house_keeper->floor->id, $rooms);

        return view('admin.roomCondition.index', compact('house_keeper', 'rooms', 'room_services'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RoomConditionRequest $request, Property $property)
    {
        try {
            DB::transaction(function () use ($request) {
                $room = Room::find($request->room_id);
                foreach ($request->room_service_id as $key => $value) {
                    RoomCondition::updateOrCreate(
                        [
                            'room_id' => $request->room_id,
                            'room_service_id' => $value,
                        ],
                        [
                            'status' => $request->status[$value],
                            'description' => $request->description[$value],
                        ]
                    );
                    $booking = BookingRoom::whereNull('departureDateTime')
                    ->whereNull('arrivalDateTime')
                    ->where('room_id', $request->room_id)
                    ->first(['id']);
                    $room->update([
                        'status' => $request->status[$value] != 'satisfied' ? 'occupied' : (!empty($booking) ? 'booked' : 'available')
                    ]);
                }
            });
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }


    /**
     * Display the specified resource.
     */
    public function show(RoomCondition $roomCondition)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    // here we are using "$roomId", because frontend we are setting the "rowid" to "room's id" but not the "roomCondition's id"
    public function edit(Request $request, Property $property, $roomId)
    {
        // dd($roomId);
        $room_condition = RoomCondition::where('room_id', $roomId)->get();

        // dd($room_condition);

        return response()->json($room_condition);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(RoomConditionRequest $request, Property $property, RoomCondition $roomCondition)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Property $property, RoomCondition $roomCondition)
    {
        //
    }
}
