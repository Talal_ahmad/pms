<?php

namespace App\Http\Controllers;

use App\Models\PropertyType;
use Illuminate\Http\Request;
use DataTables;
use DB;

class PropertyTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return DataTables::eloquent(PropertyType::query())->addIndexColumn()->make(true);
        }
        return view('admin.propertyTypes.index');

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'name' => 'required'
            ]);
            DB::transaction(function () use ($request) {
                PropertyType::create(['name' => $request->name]);
            });
            return ['code'=>'200', 'status'=>'success'];

        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(PropertyType $propertyType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(PropertyType $propertyType)
    {
        return response()->json($propertyType);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, PropertyType $propertyType)
    {
        try{
            $this->validate($request , [
                'name' => 'required'
            ]);

            $propertyType->update(['name' => $request->name]);

            return ['code'=>'200', 'status'=>'success'];

        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(PropertyType $propertyType)
    {
        try{
            $propertyType->delete();
            return ['code'=>'200', 'status'=>'success'];

        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message' => $e->getMessage()];
            }
        }
    }
}
