<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Property;
use DB;
use DataTables;
use App\Models\MenuItem;
use App\Models\MenuItemDetail;
use App\Models\MenuCategory;
use App\Models\MenuServing;
use Illuminate\Http\Request;
use App\Http\Requests\MenuItemRequest;
use Illuminate\Support\Facades\Storage;

class MenuItemController extends Controller
{
    /**
     * Display a listing of the resource. 
     */
    public function index(Request $request, Property $property)
    {
        $Menu_Category = MenuCategory::where('property_id', $property->id)->get();
        $MenuServing = MenuServing::where('is_quantity', 0)->where('property_id', $property->id)->get();
        $MenuServing_quality = MenuServing::where('is_quantity', 1)->where('property_id', $property->id)->get();
        $menuItems = MenuItem::join('menu_categories', 'menu_categories.id', '=', 'menu_items.menu_category_id')
            ->select('menu_items.*', 'menu_categories.name as categorie_name')->where('menu_categories.property_id', $property->id);
        if ($request->ajax()) {
            return DataTables::of($menuItems)->addIndexColumn()->make(true);
        }
        return view('admin.menuItem.index', compact('Menu_Category', 'MenuServing', 'MenuServing_quality'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MenuItemRequest $request)
    {
        try {
            DB::beginTransaction();
            // Validated data from the request
            $validatedData = $request->validated();

            if ($request->hasFile('pic')) {
                $imagePath = $request->file('pic')->store('images', 'public');
                $validatedData['image'] = $imagePath;
            }

            // Create a new menu item
            $menuItem = MenuItem::create([
                'name' => $validatedData['name'],
                'menu_category_id' => $validatedData['menu_category_id'],
                'image' => $validatedData['image'] ?? null,
                // Other fields you might have in your MenuItem model
            ]);

            // Loop through the servings data and create MenuItemDetail records
            foreach ($validatedData['servings'] as $detailData) {
                \Log::debug(['detailData' => $detailData]);

                $menuItem->menuItemDetails()->create([
                    'menu_serving_id' => $detailData['menu_serving_id'],
                    'menu_serving_quantity_id' => $detailData['menu_serving_quantity_id'],
                    'price' => $detailData['price'],
                    // Other fields you might have in your MenuItemDetail model
                ]);
            }

            // Additional logic or response as needed
            DB::commit();
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            DB::rollBack();
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(MenuItem $menuItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Property $property, MenuItem $menuItem)
    {

        $menuItems = MenuItem::with('menuItemDetails')
            ->find($menuItem->id);
        $menuItemDetails = $menuItems->menuItemDetails;

        return response()->json([
            'menuItems' => $menuItems,
            'menuItemDetails' => $menuItemDetails,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MenuItemRequest $request, Property $property, MenuItem $menuItem)
    {
        try {
            DB::beginTransaction();
            $validatedData = $request->validated();

            // Handle image upload for edit
            if ($request->hasFile('pic')) {
                $imagePath = $request->file('pic')->store('images', 'public');
                $validatedData['image'] = $imagePath;
            }

            // Update menu item information
            $menuItem->update([
                'name' => $validatedData['name'],
                'menu_category_id' => $validatedData['menu_category_id'],
                'image' => $validatedData['image'] ?? $menuItem->image, // Use existing image if not updated
            ]);

            // Delete existing MenuItemDetails and recreate with updated data
            $menuItem->menuItemDetails()->delete();

            foreach ($validatedData['servings'] as $detailData) {
                $menuItem->menuItemDetails()->create([
                    'menu_serving_id' => $detailData['menu_serving_id'],
                    'menu_serving_quantity_id' => $detailData['menu_serving_quantity_id'],
                    'price' => $detailData['price'],
                ]);
            }

            DB::commit();
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            DB::rollBack();
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Property $property,MenuItem $menuItem)
    {
        try {
            // Delete associated MenuItemDetails
            $menuItem->menuItemDetails()->delete();

            // Delete the MenuItem
            $menuItem->delete();

            // Delete the MenuItem
            $menuItem->delete();

            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
