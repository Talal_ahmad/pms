<?php

namespace App\Http\Controllers;

use App\Models\BookingRoom;
use Illuminate\Http\Request;
use App\Models\OrderDetail;
use App\Models\Property;
use DataTables;

use Carbon\Carbon;

class ReportController extends Controller
{
    public function saleReport(Request $request, Property $property)
    {
        if ($request->ajax()) {
            $reportData = OrderDetail::join('orders', 'orders.id', '=', 'order_details.order_id')
                ->join('menu_items', 'menu_items.id', '=', 'order_details.menu_item_id')
                ->leftjoin('menu_servings', 'menu_servings.id', '=', 'order_details.menu_serving_id')
                ->where('orders.property_id', '=', $property->id);

            if ($request->fromDate && $request->toDate) {
                $reportData = $reportData->whereDate('order_details.created_at', '>=', date('Y-m-d H:i:s', strtotime($request->fromDate)))
                    ->whereDate('order_details.created_at', '<=', date('Y-m-d H:i:s', strtotime($request->toDate)));
            }

            $reportData = $reportData->select(
                'order_details.*',
                'menu_items.name as itemName',
                'menu_servings.name as servingName',
            );

            return DataTables::of($reportData)->addIndexColumn()->make(true);
        }

        return view('admin.report.sale');
    }
    public function policeReport(Request $request, Property $property)
    {
        if ($request->ajax()) {
            $reportData = BookingRoom::join('rooms', 'rooms.id', '=', 'booking_rooms.room_id')
                ->join('customers', 'customers.id', '=', 'booking_rooms.customer_id')
                ->join('customer_types', 'customer_types.id', '=', 'booking_rooms.customer_type_id')
                ->where('customers.property_id', '=', $property->id);

            if ($request->fromDate && $request->toDate) {
                $reportData = $reportData->whereDate('booking_rooms.created_at', '>=', date('Y-m-d H:i:s', strtotime($request->fromDate)))
                    ->whereDate('booking_rooms.created_at', '<=', date('Y-m-d H:i:s', strtotime($request->toDate)));
            }

            $reportData = $reportData->select(
                'booking_rooms.*',
                'customers.name as customerName',
                'customers.cnic as customercnic',
                'rooms.name as roomName',
            );


            return DataTables::of($reportData)->addIndexColumn()->make(true);
            // dd($reportData);
        }

        return view('admin.report.police');
    }
}
