<?php

namespace App\Http\Controllers;

use App\Http\Requests\HouseKeeperRequest;
use App\Models\Floor;
use App\Models\HouseKeeperRoom;
use App\Http\Requests\StoreHouseKeeperRequest;
use App\Http\Requests\UpdateHouseKeeperRequest;
use App\Models\Property;
use App\Models\Room;
use App\Models\User;
use Illuminate\Http\Request;
use DataTables;
use Auth;
use DB;

class HouseKeeperController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Property $property)
    {
        // dd(User::find(2)->houseKeeperRooms);
        $houseKeepers = User::role('House Keeper');
        if ($request->ajax()) {
            return DataTables::of($houseKeepers)
            ->addColumn('rooms', function($row) {
                $html = '';
                foreach ($row->houseKeeperRooms as $key => $room) {
                    $html .= '<span class="badge bg-primary">'.$room->floor->name.' - '.$room->room->name.'</span> ';
                }
                return $html;
            })->rawColumns(['rooms'])->addIndexColumn()->make(true);
        }
        $floors = $property->floors;
        $houseKeepers = $houseKeepers->get(['id','name']);
        return view('admin.houseKeeper.index', compact('houseKeepers', 'floors'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(HouseKeeperRequest $request)
    {
        try {
            // dd($request->all());
            $rooms = Room::whereIn('floor_id',$request->floor_ids)->whereIn('id',$request->rooms)->get(['id','floor_id','name']);
            foreach ($rooms as $key => $room) {
                HouseKeeperRoom::create(['user_id' => $request->user_id,'floor_id' => $room->floor_id,'room_id' => $room->id]);
            }
            $fcmTokens = User::where('id', $request->user_id)->pluck('UserToken')->toArray();
            $user = Auth::user();

            $floors = Floor::whereIn('id', $request->input('floor_ids'))->pluck('name')->toArray();
            $rooms = Room::whereIn('id', $request->input('rooms'))->pluck('name')->toArray();

            $message = 'Floors of ' . implode(', ', $floors) . ', Rooms of' . implode(', ', $rooms);

            // dd($message);

            $data = [
                'title' => $user->name,
                'body' => 'Add House Keeper of ' . $message,
                // 'icon' => $icon,
                'url' => url('housekeeper'),
                'priority' => 'normal'
            ];
            $notification = sendNotifications($fcmTokens, $data);
            if ($notification == 200) {
                return ['code' => 200, 'status' => 'success', 'message' => 'Notification send successfully!'];
            } else {
                return ['code' => $notification, 'status' => 'failed', 'message' => 'Notification Error! Please Contact Administrator.'];
            }
            // return ['code' => '200', 'status' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(HouseKeeperRoom $houseKeeper)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    // public function edit(HouseKeeper $houseKeeper)
    public function edit(Property $property, $id)
    {
        $floor_ids = User::find($id)->houseKeeperRooms()->pluck('floor_id')->toArray();
        $room_ids = User::find($id)->houseKeeperRooms()->pluck('room_id')->toArray();
        return response()->json(['user' => $id, 'floor_ids' => $floor_ids, 'room_ids' => $room_ids]);
    }

    /**
     * Update the specified resource in storage.
     */
    // public function update(HouseKeeperRequest $request, HouseKeeper $houseKeeper)
    public function update(HouseKeeperRequest $request, Property $property, $id)
    {
        try {
            HouseKeeperRoom::where('user_id',$request->user_id)->delete();
            $rooms = Room::whereIn('id',$request->rooms)->get(['id','floor_id','name']);
            foreach ($rooms as $key => $room) {
                HouseKeeperRoom::create(['user_id' => $request->user_id,'floor_id' => $room->floor_id,'room_id' => $room->id]);
            }
            return ['code' => '200', 'status' => 'success'];

        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    // public function destroy(Request $request, Property $property, HouseKeeper $houseKeeper)
    public function destroy(Request $request, Property $property, HouseKeeperRoom $housekeeper)
    {
        try {
            $housekeeper->delete();

            return ['code' => '200', 'status' => 'success'];

        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
    /**
     * when a Floor is selected, get its Rooms (not working , and not being used)
     */
    // public function getRooms(Property $property, Request $request)
    // {
    //     dd($request);

    //     if ($request->ajax()) {

    //         dd($request);
    //         // dd(Floor::all());
    //         // dd($request->floor_id);
    //         $rooms = Floor::find($request->floor_id)->rooms;
    //         return response()->json($rooms);
    //     }
    // }
}
