<?php

namespace App\Http\Controllers;
use App\Models\MenuCategory;
use App\Models\Property;
use App\Models\MenuServing;
use DataTables;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests\MenuServingRequest;


class MenuServingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request,Property $property)
    {
        $Menu_Category = MenuCategory::where('property_id', $property->id)->get();
        $menuServings = MenuServing::where('property_id', $property->id)->get();
        if($request->ajax()){
            return DataTables::of($menuServings)->addIndexColumn()->make(true);
        }
        return view('admin.menuServing.index',compact('Menu_Category'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MenuServingRequest $request)
    {
        // dd($request->all());
        try{
            $propertyId = $request->input('property_id');

            $property = Property::findOrFail($propertyId);
            // Merge the property_id into the request data
            $request->merge(['property_id' => $property->id]);

            DB::transaction(function () use ($request) {

                MenuServing::create($request->validated());
            });
            return ['code'=>'200', 'status'=>'success'];

        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(MenuServing $menuServing)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Property $property, MenuServing $menuServing)
    {
        return response()->json($menuServing);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MenuServingRequest $request, Property $property, MenuServing $menuServing)
    {
        try {
            $requestData = $request->merge(['property_id' => $property->id]);

            DB::transaction(function () use ($requestData, $menuServing) {
                $menuServing->update($requestData->all());
            });
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Property $property, MenuServing $menuServing)
    {
        try {
            $menuServing->delete();
    
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
