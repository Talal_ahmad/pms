<?php

namespace App\Http\Controllers;

use App\Models\Property;
use App\Http\Requests\VehicleTypeRequest;
use DataTables;
use DB;
use App\Models\VehicleType;
use Illuminate\Http\Request;

class VehicleTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Property $property)
    {
        $vehicle_Type = $property->VehicleTypes;
        if ($request->ajax()) {
            return DataTables::of($vehicle_Type)->addIndexColumn()->make(true);
        }
        return view('admin.vehicleType.index', compact('property'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(VehicleTypeRequest $request)
    {
        try {
            $propertyId = $request->input('property_id');

            $property = Property::findOrFail($propertyId);
            // Merge the property_id into the request data
            $request->merge(['property_id' => $property->id]);

            DB::transaction(function () use ($request) {
                vehicleType::create($request->validated());
            });
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(VehicleType $vehicleType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Property $property, VehicleType $vehicleType)
    {
        return response()->json($vehicleType);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(VehicleTypeRequest $request, Property $property, VehicleType $vehicleType)
    {
        try {
            $requestData = $request->merge(['property_id' => $property->id]);

            DB::transaction(function () use ($requestData, $vehicleType) {
                $vehicleType->update($requestData->all());
            });
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Property $property, VehicleType $vehicleType)
    {
        try {
            $vehicleType->delete();

            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
