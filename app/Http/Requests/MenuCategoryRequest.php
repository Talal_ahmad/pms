<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MenuCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'property_id' => 'required',
            'name' => 'required|unique:menu_categories',
        ];

        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $menu_category = $this->route()->parameter('menu_category');
            if ($menu_category) {
                $rules['name'] = 'required|unique:menu_categories,name,' . $menu_category->id;
            }
        }

        return $rules;
    }
}
