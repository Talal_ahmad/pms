<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingRoomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            // user
            'name' => 'required|string',
            'email' => 'nullable|email',
            'phone_number' => 'required|string',
            'cnic' => 'required|string',
            'address' => 'required|string',
            // Customer
            'description' => 'nullable|string',
            'gender' => 'nullable|in:male,female,other',
            'guest_category' => 'nullable|string',
            'cnic_expired' => 'nullable|date',
            'nationality' => 'nullable|string',
            'dob' => 'nullable|date',
            'country_code' => 'string',
            // booing Room
            'customer_type_id' => 'nullable|exists:customer_types,id',
            'reference_id' => 'nullable|exists:references,id',
            'fromDateTime' => 'required|date',
            'toDateTime' => 'required|date|after:fromDateTime',
            'company_reference_id' => 'nullable|exists:company_references,id',
            'room_id' => 'required|exists:rooms,id',
            // Add specific validation rules for complementary_food
            'complementary_food' => 'nullable',
            // 'complementary_food.breakfast' => 'nullable|boolean',
            // 'complementary_food.lunch' => 'nullable|boolean',
            // 'complementary_food.dinner' => 'nullable|boolean',

            //  'adult_count' => 'required|integer|min:0',
            //  'child_count' => 'required|integer|min:0',
            //  'infant_count' => 'required|integer|min:0',
            //  'night_number' => 'required|integer|min:1',


        ];

        // If the request method is PUT or PATCH, apply additional rules
        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $booking = $this->route()->parameter('booking');
            $rules['name'] = 'required|string';
            // Add other rules for updating if needed
        }

        return $rules;
    }
}
