<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoomConditionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'room_id' => 'required|exists:rooms,id',
            'room_service_id' => 'required|array',
            'room_service_id.*' => 'required|exists:room_services,id',
            'status' => 'required|array',
            'status.*' => 'required|in:satisfied,need_repairing,dirty',
            'description' => 'required|array',
            'description.*' => 'required_if:status.*,need_repairing,dirty',
        ];

        return $rules;

    }
}
