<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CheckTableReservation;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            // 'customer' => 'required',
            'total_price' => 'required',
            'orderedItems' => 'required|array',
            'orderedItems.*.id' => 'required|integer',
            'orderedItems.*.menu_serving_id' => 'nullable|exists:menu_servings,id',
            'orderedItems.*.menu_serving_quantity_id' => 'nullable|exists:menu_servings,id',
            'orderedItems.*.quantity' => 'required',
            'orderedItems.*.price' => 'required',
            'orderedItems.*.total_price' => 'required'
        ];

        return $rules;
    }
}
