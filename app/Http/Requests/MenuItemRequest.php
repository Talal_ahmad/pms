<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MenuItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'menu_category_id' => 'required|exists:menu_categories,id',
            'name' => ['required', Rule::unique('menu_items', 'name')->where('menu_category_id', $this->menu_category_id)],
            'servings' => 'required|array',
            'servings.*.menu_serving_id' => 'nullable|exists:menu_servings,id',
            'servings.*.menu_serving_quantity_id' => 'nullable|exists:menu_servings,id',
            'servings.*.price' => 'required|numeric',
        ];

        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $menuItem = $this->route()->parameter('menu_item');
            \Log::debug(['item' => $menuItem]);
            $rules ['name'] =  [
                'required',
                Rule::unique('menu_items', 'name')->where('menu_category_id', $this->menu_category_id)->ignore($menuItem->id),
            ];
        }

        return $rules;
    }
}
