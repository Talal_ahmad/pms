<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MenuServingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'property_id' => 'required',
            'name' => 'required|unique:menu_servings',
            'menu_category_id' => 'nullable|exists:menu_categories,id',
            'is_quantity' => 'nullable|boolean',
        ];

        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $menu_serving = $this->route()->parameter('menu_serving');
            if ($menu_serving) {
                $rules['name'] = 'required|unique:menu_servings,name,' . $menu_serving->id;
            }
        }

        return $rules;
    }
}
