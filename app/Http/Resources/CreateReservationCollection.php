<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Carbon\Carbon;

class CreateReservationCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        return $this->collection->map(function ($table) {
            return [
                'id' => $table->id,
                'name' => $table->name,
                'reservations' => $table->reservations->map(function ($reservation) {
                    $startDateTime = Carbon::parse($reservation->start_date_time);
                    $endDateTime = Carbon::parse($reservation->end_date_time);
                    return [
                        'id' => $reservation->id,
                        'end_date_time' => $endDateTime->format('Y-m-d H:i:s'),
                        'dateTime' => $startDateTime->format('j F Y g:i A') . ' to ' . $endDateTime->format('j F Y g:i A'),
                        'customer' => $reservation->customer->name
                    ];
                }),
            ];
        })->all();
    }
}
