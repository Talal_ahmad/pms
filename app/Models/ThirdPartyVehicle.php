<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ThirdPartyVehicle extends Model
{
    use HasFactory;
    protected $table = 'third_party_vehicles';


    protected $fillable = [
        'company_name',
        'email_address',
        'phone_number',
        'address',
        'company_owner_name',
        // Add any other fields
    ];
}
