<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class OrderDetail extends Model implements Auditable
{
    use HasFactory, \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'order_id',
        'menu_item_id',
        'menu_serving_id',
        'menu_serving_quantity_id',
        'price',
        'quantity',
        'total_price',
    ];

    public function menuItem()
    {
        return $this->belongsTo(MenuItem::class);
    }

    public function menuServing()
    {
        return $this->belongsTo(MenuServing::class)->where('is_quantity', 0);
    }

    public function menuServingQuantity()
    {
        return $this->belongsTo(MenuServing::class, 'menu_serving_quantity_id', 'id')->where('is_quantity', 1);
    }
}
