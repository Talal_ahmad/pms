<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reference extends Model
{
    use HasFactory;
    protected $fillable = [
        'reference_name',
        'email_address',
        'phone_number',
        'address',
        'discount',
        'reference_commission',
        // Add any other fields
    ];
}
