<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyReference extends Model
{
    use HasFactory;
    protected $fillable = [
        'company_name',
        'email_address',
        'phone_number',
        'address',
        'discount',
        'company_commission',
        // Add any other fields
    ];
}
