<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleBooking extends Model
{
    use HasFactory;

    protected $fillable = [
        'property_id',
        'booking_id',
        'vehicle_id',
        'fromRentDateTime',
        'toRentDateTime',
        'package_type',
    ];
}
