<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class HouseKeeperRoom extends Model
{
    use HasFactory;
    protected $fillable = ['user_id','floor_id','room_id'];

    // public function user(): BelongsTo
    // {
    //     return $this->belongsTo(User::class);
    // }
    public function floor(): BelongsTo
    {
        return $this->belongsTo(Floor::class);
    }

    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class);
    }
}
