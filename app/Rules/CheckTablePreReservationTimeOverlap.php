<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Reservation;
use Carbon\Carbon;

class CheckTablePreReservationTimeOverlap implements Rule
{
    protected $tableId;
    protected $fromDateTime;
    protected $endDateTime;
    protected $table;

    public function __construct($tableId, $fromDateTime, $endDateTime, $table = null)
    {
        $this->tableId = $tableId;
        $this->fromDateTime = Carbon::parse($fromDateTime);
        $this->endDateTime = Carbon::parse($endDateTime);
        $this->table = $table;
    }

    public function passes($attribute, $value)
    {
        // Check for existing reservations for the same table with overlapping time
        $existingReservations = Reservation::where('table_id', $this->tableId)
            ->where(function ($query) {
                $query->where(function ($q) {
                    $q->where('start_date_time', '>=', $this->fromDateTime)
                        ->where('start_date_time', '<', $this->endDateTime);
                })->orWhere(function ($q) {
                    $q->where('end_date_time', '>', $this->fromDateTime)
                        ->where('end_date_time', '<=', $this->endDateTime);
                });
            });
            if($this->table){
                $existingReservations = $existingReservations->where('table_id', '!=', $this->table);
            }
            $existingReservations = $existingReservations->first();

        return !$existingReservations;
    }

    public function message()
    {
        return 'A reservation already exists for this table against the selected time.';
    }
}
