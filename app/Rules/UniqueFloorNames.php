<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class UniqueFloorNames implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     * @return void
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        // Get the floor names from the request data
        $floorNames = collect(request()->input('floors', []))->pluck('name');

        // Check if floor names are unique
        $uniqueFloorNames = $floorNames->unique();

        if ($floorNames->count() !== $uniqueFloorNames->count()) {
            $fail("The floor name on $attribute is repeated more than once.");
        }
    }
}
