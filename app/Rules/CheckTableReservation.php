<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\Rule;

class CheckTableReservation implements Rule
{
    public function passes($attribute, $value)
    {
        $tableStatus = \DB::table('tables')->where('id', $value)->value('status');

        if ($tableStatus !== 'occupied') {
            return false;
        }
        return true;
    }

    public function message()
    {
        return 'Table is not Occupied.';
    }
}
