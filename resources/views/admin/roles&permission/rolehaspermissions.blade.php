@extends('admin.layouts.master')
@section('title', 'Assign Pemissions')

@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-2 mb-1"><span class="text-muted fw-light">Role /</span> Assign Permissions</h4>
        <div class="card">
            <div class="card-header bg-light text-white mb-1">
                <h3 class="card-title">{{ $role->name }}
                    <button id="checkAllBtn" style="float: inline-end" class="btn btn-sm btn-light"></button>
                </h3>
            </div>
            <div class="card-body mt-2">
                <form class="form" action="{{ route('store-role-permissions', $role->id) }}" method="POST">
                    @csrf
                    @foreach ($permissions_parent_name as $parent_name)
                        <div class="row mb-3">
                            <div class="col-md-12">
                                <div class="mb-2">
                                    <h4 class="section-title">
                                        <input class="custom-control-input parent-checkbox" type="checkbox"
                                            id="parent_{{ $parent_name->parent_name }}"
                                            data-parent-name="{{ $parent_name->parent_name }}">
                                        <label class="custom-control-label"
                                            for="parent_{{ $parent_name->parent_name }}">{{ $parent_name->parent_name }}</label>
                                    </h4>
                                </div>
                            </div>
                            @php
                                $permissions = permissionParentName($parent_name->parent_name);
                            @endphp
                            <div class="col-md-12">
                                @foreach ($permissions as $permission)
                                    @php
                                        if (in_array($permission->id, $role_permissions)) {
                                            $check = 'checked';
                                        } else {
                                            $check = '';
                                        }
                                    @endphp
                                    <div class="custom-control custom-checkbox mb-2">
                                        <input class="custom-control-input child-checkbox" type="checkbox"
                                            id="{{ $permission->display_name }}" name="permissions[]"
                                            value="{{ $permission->id }}" data-parent="{{ $parent_name->parent_name }}"
                                            {{ $check }} />
                                        <label class="custom-control-label"
                                            for="{{ $permission->display_name }}">{{ $permission->display_name }}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <hr>
                    @endforeach
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            var parentCheckboxes = document.querySelectorAll('.parent-checkbox');

            parentCheckboxes.forEach(function(parentCheckbox) {
                parentCheckbox.addEventListener('click', function() {
                    var parentChecked = parentCheckbox.checked;
                    var parentName = parentCheckbox.dataset.parentName;
                    var childCheckboxes = document.querySelectorAll(
                        '.child-checkbox[data-parent="' + parentName + '"]');

                    childCheckboxes.forEach(function(childCheckbox) {
                        childCheckbox.checked = parentChecked;
                    });

                    updateCheckAllButton();
                });
            });
        });

        document.getElementById('checkAllBtn').addEventListener('click', function() {
            var checkboxes = document.querySelectorAll('input[type="checkbox"]');
            var allChecked = true;

            checkboxes.forEach(function(checkbox) {
                checkbox.checked = !checkbox.checked;
                if (!checkbox.checked) {
                    allChecked = false;
                }
            });

            updateCheckAllButton(allChecked);
        });

        function updateCheckAllButton(allChecked) {
            var button = document.getElementById('checkAllBtn');

            if (allChecked) {
                button.textContent = 'Uncheck All';
            } else {
                button.textContent = 'Check All';
            }
        }
    </script>
@endsection
