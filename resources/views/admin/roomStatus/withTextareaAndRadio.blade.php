@extends('admin.layouts.master')
@section('title', 'Room Condition')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-2 mb-1"><span class="text-muted fw-light">Room Status /</span> List</h4>
        {{-- <div class="card">
            <div class="card-datatable table-responsive">

                <div class="col-12 px-3">

                </div>

            </div>

        </div> --}}

        <div class="card mt-1">
            <div class="m-3 rounded">
                <div class="container">
                    <div class="row">
                        <ul class="nav nav-pills mt-1" id="pills-tab" role="tablist">
                            <div>
                                <h3 class="mb-0 " style="font-family: Montserrat; font-weight: 600; font-size: 24px;">
                                    Floors:
                                </h3>
                            </div>
                            @foreach ($floors as $floor)
                                <li class="nav-item ms-4 mx-2 mb-0" role="presentation">
                                    <a class="nav-link mb-0 btn rounded-pill fw-bold{{ $loop->first ? ' active border-dark' : '' }}"
                                        id="floor-{{ $floor->id }}-tab" data-bs-toggle="pill"
                                        data-bs-target="#floor-{{ $floor->id }}" type="button" role="tab"
                                        aria-controls="floor-{{ $floor->id }}" aria-selected="{{ $loop->first }}">
                                        {{ $floor->name }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div id="myDIV" class="row">
                        <div class="tab-content" id="pills-tabContent">
                            @foreach ($floors as $floor)
                                <div class="tab-pane fade show{{ $loop->first ? ' active' : '' }}"
                                    id="floor-{{ $floor->id }}" role="tabpanel">
                                    <div class="row mt-1">
                                        @foreach ($floor->rooms as $key => $room)

                                            @php
                                                $roomConditionsQuery = $room->roomConditions();
                                                $needRepairing = $roomConditionsQuery->where('status', 'need_repairing')->exists();
                                                // $roomConditionsQuery = $room->roomConditionsQuery()->get();
                                                if($key == 1) {
                                                    // dd($room);
                                                    // dd($roomConditionsQuery->get()->loadMissing('roomService'));
                                                    // dd($room->roomConditionsQuery()->get()->loadMissing('roomService'));
                                                    // dd($roomConditionsQuery->get());
                                                    // dd( $roomConditionsQuery->select('id', 'status')->get());
                                                    // dd($needRepairing);
                                                }
                                            @endphp
                                        
                                            <div class="col-12 col-sm-6 col-md-3 col-lg-3 rounded"
                                                style="border-radius: 10px;">
                                                <div class="card p-0 rounded-4 mt-4 text-dark {{ $needRepairing ? 'bg-danger text-white cursor-pointer' : '' }}"
                                                    data-bs-toggle="modal" data-bs-target="#add_modal_{{ $room->id }}"
                                                    style="border-radius: 10px;">
                                                    {{-- <div>
                                                        <div class="row">
                                                            <div class="col-7"></div>
                                                            <div class="col-4 d-flex justify-content-center rounded-bottom text-white mb-0 pb-0"
                                                                style="background-color: #56C456;">
                                                                <p class="mb-0 pb-1">Available</p>
                                                            </div>
                                                        </div>
                                                    </div> --}}
                                                    <div class="mt-3 mb-0 pb-0 d-flex justify-content-center text-"
                                                        style=" border-top-right-radius: 10px;border-top-left-radius:10px ">
                                                        <div class="d-flex justify-content-center text-  "
                                                            style="font-family: Poppins; font-weight: 500; font-size: 16px;">
                                                            {{ $room->name }}
                                                        </div>
                                                    </div>
                                                    <div class="card-body m-0 p-0">
                                                        <div>
                                                            <div class="d-flex justify-content-center"
                                                                style="height: 190px;"><img
                                                                    src="{{ asset('images/roomBlue.png') }}"
                                                                    alt="img">
                                                            </div>
                                                        </div>
                                                        {{-- <div class="d-flex justify-content-center ">
                                                            <div>
                                                                <button type="button"
                                                                    class="btn btn-success px-5 rounded border-0 mb-4 mt-2 "
                                                                    style="background-color: #6366F1; font-family: Poppins; font-weight: 400; font-size: 15px;">Reserve
                                                                    Room
                                                                </button>
                                                            </div>
                                                        </div> --}}
                                                    </div>
                                                </div>
                                            </div>

                                            @if ($needRepairing)
                                                
                                                <!-- Add New Modal -->
                                                <div class="modal fade" id="add_modal_{{ $room->id }}" tabindex="-1"
                                                    aria-hidden="true">
                                                    <div
                                                        class="modal-dialog modal-dialog-centered1 modal-lg modal-simple modal-add-new-cc">
                                                        <div class="modal-content p-3 px-md-5 py-md-2">
                                                            <div class="modal-body pb-0">
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                                    aria-label="Close"></button>
                                                                <div class="text-center mb-5">
                                                                    <h3 class="mb-2">Room Condition</h3>
                                                                </div>
                                                                {{-- <form id="add_form" class="form row g-3">
                                                                    @csrf

                                                                    <input type="hidden" id="room_id" name="room_id"> --}}
                                                                    @php
                                                                        $roomConditions = $roomConditionsQuery->get();
                                                                        // dd($room_services[0]);
                                                                        // $roomConditions = $roomConditionsQuery->select('id', 'status')->get();
                                                                        // $roomConditions = $roomConditionsQuery->select('id', 'status')->get()->loadMissing('roomService');
                                                                        if ($room->id == 2) {

                                                                            // dd($roomConditions[0]->roomService);
                                                                            // dd($roomConditions[0], $room_services[0]);
                                                                            // dd($roomConditions[2], $room_services[$roomConditions[2]->room_service_id-1]);
                                                                        }
                                                                    @endphp

                                                                    {{-- @dd($roomConditions->isEmpty()) --}}
                                                                    {{-- @dd($roomConditions->isNotEmpty()) --}}
                                                                    {{-- @dd($roomConditions->isNotEmpty() ? "not null" : "null") --}}
                                                                    {{-- @dd($roomConditions[0]['status']) --}}

                                                                    @foreach ($roomConditions as $key => $roomCondition)

                                                                        @php
                                                                            $room_service_id = $roomCondition->room_service_id;
                                                                            $room_service_name = $room_services[$room_service_id-1]->name;
                                                                        @endphp
                                                                    
                                                                        <div class="col-12 d-flex flex-wrap">
                                                                            <input type="hidden" name="room_service_id[]"
                                                                                value="{{ $room_service_id }}">

                                                                            <h4 class="col-6">
                                                                                {{ $room_service_name }}
                                                                            </h4>

                                                                            <div class="col-2 form-check form-check-inline">
                                                                                <label class="form-check-label"
                                                                                    for="{{ $room_service_name }}1">
                                                                                    Satisfied
                                                                                </label>
                                                                                <input class="form-check-input" type="radio"
                                                                                    name="status[{{ $room_service_id }}]"
                                                                                    id="{{ $room_service_name }}1"
                                                                                    value="satisfied"
                                                                                    
                                                                                    {{ $roomConditions->isNotEmpty() ? ($roomConditions[$key]['status'] == "satisfied" ? "checked" : '') : '' }}
                                                                                    
                                                                                    {{-- check it: its not working for default value of check for satisfied --}}
                                                                                    {{-- {{ $roomConditions->isNotEmpty() ? ($roomConditions[$key]['status'] == "satisfied" ? "checked" : '') : "checked" }} --}}

                                                                                >
                                                                            </div>

                                                                            <div class="col-3 form-check form-check-inline">
                                                                                <label class="form-check-label"
                                                                                    for="{{ $room_service_name }}2">
                                                                                    Need Repairing
                                                                                </label>
                                                                                <input class="form-check-input" type="radio"
                                                                                    name="status[{{ $room_service_id }}]"
                                                                                    id="{{ $room_service_name }}2"
                                                                                    value="need_repairing"

                                                                                    {{ $roomConditions->isNotEmpty() ? ($roomConditions[$key]['status'] == "need_repairing" ? "checked" : '') : '' }}

                                                                                >
                                                                            </div>
                                                                            
                                                                            <div class="col-12">
                                                                                <label for="{{ $room_service_name }}3">
                                                                                    Description:
                                                                                </label>
                                                                                <textarea class="form-control" placeholder="Leave a comment here" id="{{ $room_service_name }}3" name="description[{{ $room_service_id }}]"
                                                                                          value="{{$roomCondition->description}}"
                                                                                >
                                                                                </textarea>
                                                                            </div>

                                                                            <hr class="col-12 p-0">

                                                                        </div>
                                                                        @endforeach


                                                                    {{-- <div class="col-12 text-center">
                                                                        <button type="submit"
                                                                            class="btn btn-primary me-sm-3 me-1">Submit</button>
                                                                        <button type="reset"
                                                                            class="btn btn-label-secondary btn-reset"
                                                                            data-bs-dismiss="modal" aria-label="Close">
                                                                            Cancel
                                                                        </button>
                                                                    </div> --}}
                                                                {{-- </form> --}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/ Add New Modal -->

                                            @endif

                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                            <!-- Third Part -->
                            <div class="tab-pane fade" id="pills-contact" role="tabpanel"
                                aria-labelledby="pills-contact-tab">
                                <div class="row mt-5">
                                    <!-- ... Your existing card content ... -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="sk-wave sk-primary" id="loader"
            style="display: none; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var propertyId = '{!! request()->route('property')->id !!}';
        $(document).ready(function() {

            var url = "{{ route('room-condition.index', ['property' => ':propertyId']) }}";
            url = url.replace(':propertyId', propertyId);



            // $("#add_form").submit(function(e) {
            //     $('#loader').show();
            //     e.preventDefault();

            //     $.ajax({
            //         url: "{{ url('properties') }}" + "/" + propertyId + "/room-condition",
            //         type: "POST",
            //         data: new FormData(this),
            //         processData: false,
            //         contentType: false,
            //         responsive: true,
            //         success: function success(response) {
            //             // sweetalert
            //             if (response.errors) {
            //                 $.each(response.errors, function(index, value) {
            //                     Swal.fire({
            //                         title: value,
            //                         icon: 'error',
            //                         customClass: {
            //                             confirmButton: 'btn btn-success'
            //                         }
            //                     });
            //                 });
            //             } else if (response.error_message) {
            //                 swal.fire({
            //                     icon: 'error',
            //                     title: 'An error has been occured! Please Contact Administrator.'
            //                 })
            //             } else {
            //                 $('#add_form')[0].reset();
            //                 dataTable.ajax.reload();
            //                 $('#loader').hide();
            //                 $('#add_modal').modal('hide');
            //                 Swal.fire({
            //                     icon: 'success',
            //                     title: 'Room Condition has been Updated Successfully!',
            //                     customClass: {
            //                         confirmButton: 'btn btn-success'
            //                     }
            //                 });
            //             }
            //         },
            //     });
            // });

        });

        
    </script>
@endsection
