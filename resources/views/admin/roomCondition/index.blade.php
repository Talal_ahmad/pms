@extends('admin.layouts.master')
@section('title', 'Room Condition')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-2 mb-1"><span class="text-muted fw-light">Room Condition /</span> List</h4>

        <div class="card mt-1">
            <div class="m-3 rounded">
                <div class="container">

                    <div class="row mt-1">
                        @foreach ($rooms as $key => $room)
                            {{-- @dd($room) --}}

                            {{-- room card --}}
                            <div class="col-12 col-sm-6 col-md-3 col-lg-3 rounded" style="border-radius: 10px;">
                                <div class="card p-0 rounded-4 mt-4 text-dark" style="border-radius: 10px;">
                                    <div class="row">
                                        <div class="col-7"></div>
                                        @if ($room->status == 'dirty')
                                            <div class="col-4 d-flex justify-content-center rounded-bottom-pill  text-white mb-0 pb-0"
                                                style="background-color: #E72F60; border-radius: 0 0 13px 13px;">
                                                <p class="mb-0 pb-1">Dirty</p>
                                            </div>
                                        @else
                                            <div class="col-4 d-flex justify-content-center rounded-bottom-pill  text-white mb-0 pb-0"
                                                style="background-color: #56C456; border-radius: 0 0 13px 13px;">
                                                <p class="mb-0 pb-1">Clean</p>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3 mb-0 pb-0 d-flex justify-content-center text-"
                                        style=" border-top-right-radius: 10px;border-top-left-radius:10px ">
                                        <div class="d-flex justify-content-center text-  "
                                            style="font-family: Poppins; font-weight: 500; font-size: 16px;">
                                            {{ $room->name }}
                                        </div>
                                    </div>
                                    <div class="card-body m-0 p-0">
                                        <div>
                                            <div class="d-flex justify-content-center" style="height: 190px;"><img
                                                    src="{{ asset('images/roomBlue.png') }}" alt="img">
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-center ">
                                            <div>
                                                <button type="button"
                                                    class="btn btn-success px-5 rounded border-0 mb-4 mt-2 "
                                                    style="background-color: #6366F1; font-family: Poppins; font-weight: 400; font-size: 15px;"
                                                    data-bs-toggle="modal" data-bs-target="#add_modal"
                                                    onclick="ServingEdit('{{ $room->id }}')">
                                                    Edit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- "Update Room Condition" Modal -->
                            <div class="modal fade" id="add_modal" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered1 modal-lg modal-simple modal-add-new-cc">
                                    <div class="modal-content p-3 p-md-5">
                                        <div class="modal-body">
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                            <div class="text-center mb-4">
                                                <h3 class="mb-2">Update Room Condition</h3>
                                            </div>
                                            <form id="add_form" class="form row g-3">
                                                @csrf
                                                {{-- <div class="col-6">
                                                    <label class="form-label" for="room_service">room_services</label>
                                                    <select id="users" name="room_service" class="form-select select2"
                                                        data-placeholder="Select User">
                                                        <option value=""></option>
                                                        @foreach ($room_services as $room_service)
                                                            <option value="{{ $room_service->id }}">{{ $room_service->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div> --}}

                                                <input type="hidden" id="room_id" name="room_id">

                                                @foreach ($room_services as $room_service)
                                                    <div class="col-12 d-flex flex-wrap">
                                                        <input type="hidden" name="room_service_id[]"
                                                            value="{{ $room_service->id }}">

                                                        <h4 class="col-4 text-capitalize">
                                                            {{ $room_service->name }}
                                                        </h4>

                                                        <div class="col-2 form-check form-check-inline">
                                                            <label class="form-check-label"
                                                                for="{{ $room_service->name }}1">
                                                                Satisfied
                                                            </label>
                                                            <input class="form-check-input" type="radio"
                                                                name="status[{{ $room_service->id }}]"
                                                                id="{{ $room_service->name }}1" value="satisfied" checked>
                                                        </div>

                                                        <div class="col-3 form-check form-check-inline">
                                                            <label class="form-check-label"
                                                                for="{{ $room_service->name }}2">
                                                                Need Repairing
                                                            </label>
                                                            <input class="form-check-input" type="radio"
                                                                name="status[{{ $room_service->id }}]"
                                                                id="{{ $room_service->name }}2" value="need_repairing">
                                                        </div>

                                                        <div class="col-2 form-check form-check-inline">
                                                            <label class="form-check-label"
                                                                for="{{ $room_service->name }}3">
                                                                Dirty
                                                            </label>
                                                            <input class="form-check-input" type="radio"
                                                                name="status[{{ $room_service->id }}]"
                                                                id="{{ $room_service->name }}3" value="dirty">
                                                        </div>
                                                        <div class="col-12">
                                                            <label class="" for="{{ $room_service->name }}3">
                                                                Description:
                                                            </label>
                                                            <textarea class="form-control" placeholder="Leave a comment here" id="{{ $room_service->name }}4"
                                                                name="description[{{ $room_service->id }}]"></textarea>
                                                        </div>
                                                    </div>
                                                    <hr class="mb-0 mt-3 p-0">
                                                @endforeach


                                                <div class="col-12 text-center">
                                                    <button type="submit"
                                                        class="btn btn-primary me-sm-3 me-1">Submit</button>
                                                    <button type="reset" class="btn btn-label-secondary btn-reset"
                                                        data-bs-dismiss="modal" aria-label="Close">
                                                        Cancel
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ "Update Room Condition" Modal -->
                        @endforeach
                    </div>

                </div>
            </div>
        </div>


        <div class="card">
            {{-- <div class="card-datatable table-responsive">

                <div class="d-flex justify-content-around">
                    <div class="h5 p-3 m-0 w-auto">
                        House Keeper: <span class="fw-normal text-secondary">{{ $house_keeper->user->name }}</span>
                    </div>
                    <div class="h5 p-3 m-0 w-auto">
                        Floor: <span class="fw-normal text-secondary">{{ $house_keeper->floor->name }}</span>
                    </div>
                </div>
                
            </div> --}}

        </div>
        <div class="sk-wave sk-primary" id="loader"
            style="display: none; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var propertyId = '{!! request()->route('property')->id !!}';
        $(document).ready(function() {

            // $('.select2').select2();

            // $('#add_modal').modal('show');

            var url = "{{ route('room-condition.index', ['property' => ':propertyId']) }}";
            url = url.replace(':propertyId', propertyId);
            // dataTable = $('.datatables-users').DataTable({
            //     processing: true,
            //     serverSide: true,
            //     ajax: url,
            //     columns: [
            //         // columns according to JSON
            //         {
            //             data: 'responsive_id'
            //         },
            //         {
            //             data: 'DT_RowIndex',
            //             name: 'DT_RowIndex',
            //             orderable: false,
            //             searchable: false
            //         },
            //         {
            //             data: 'name'
            //         },
            //         {
            //             data: 'action'
            //         }
            //     ],
            //     columnDefs: [{
            //         // For Responsive
            //         className: 'control',
            //         searchable: false,
            //         orderable: false,
            //         responsivePriority: 2,
            //         targets: 0,
            //         render: function render(data, type, full, meta) {
            //             // console.log(full);
            //             return '';
            //         }
            //     }, {
            //         // Actions
            //         targets: -1,
            //         title: 'Actions',
            //         searchable: false,
            //         orderable: false,
            //         render: function render(data, type, full, meta) {
            //             // console.log(data);
            //             // console.log(full.id);
            //             return '<div class="d-inline-block text-nowrap">' +
            //                 '<a href="javascript:;" class="text-body" onclick=ServingEdit(' +
            //                 full.id + ')>' + '<i class=\"ti ti-edit\"></i>' +
            //                 '</a>';
            //         }
            //     }],
            //     order: [
            //         [2, 'desc']
            //     ],
            //     dom: '<"row mx-2"' + '<"col-md-2"<"me-3"l>>' +
            //         '<"col-md-10"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"fB>>' +
            //         '>t' + '<"row mx-2"' + '<"col-sm-12 col-md-6"i>' + '<"col-sm-12 col-md-6"p>' + '>',
            //     language: {
            //         sLengthMenu: '_MENU_',
            //         search: '',
            //         searchPlaceholder: 'Search..'
            //     },
            //     // Buttons with Dropdown
            //     buttons: [{
            //         extend: 'collection',
            //         className: 'btn btn-label-primary dropdown-toggle mx-3',
            //         text: '<i class="ti ti-logout rotate-n90 me-2"></i>Export',
            //         buttons: [{
            //             extend: 'print',
            //             title: 'Users',
            //             text: '<i class="ti ti-printer me-2" ></i>Print',
            //             className: 'dropdown-item',
            //             exportOptions: {
            //                 columns: [2, 3],
            //                 // prevent avatar to be print
            //                 format: {
            //                     body: function body(inner, coldex, rowdex) {
            //                         if (inner.length <= 0) return inner;
            //                         var el = $.parseHTML(inner);
            //                         var result = '';
            //                         $.each(el, function(index, item) {
            //                             if (item.classList !== undefined && item
            //                                 .classList.contains('user-name')) {
            //                                 result = result + item.lastChild
            //                                     .textContent;
            //                             } else result = result + item.innerText;
            //                         });
            //                         return result;
            //                     }
            //                 }
            //             },
            //             customize: function customize(win) {
            //                 //customize print view for dark
            //                 $(win.document.body).css('color', config.colors
            //                         .headingColor)
            //                     .css('border-color', config.colors.borderColor).css(
            //                         'background-color', config.colors.body);
            //                 $(win.document.body).find('table').addClass('compact').css(
            //                         'color', 'inherit').css('border-color', 'inherit')
            //                     .css(
            //                         'background-color', 'inherit');
            //             }
            //         }, {
            //             extend: 'csv',
            //             title: 'Users',
            //             text: '<i class="ti ti-file-text me-2" ></i>Csv',
            //             className: 'dropdown-item',
            //             exportOptions: {
            //                 columns: [2, 3],
            //                 // prevent avatar to be print
            //                 format: {
            //                     body: function body(inner, coldex, rowdex) {
            //                         if (inner.length <= 0) return inner;
            //                         var el = $.parseHTML(inner);
            //                         var result = '';
            //                         $.each(el, function(index, item) {
            //                             if (item.classList.contains(
            //                                     'user-name')) {
            //                                 result = result + item.lastChild
            //                                     .textContent;
            //                             } else result = result + item.innerText;
            //                         });
            //                         return result;
            //                     }
            //                 }
            //             }
            //         }, {
            //             extend: 'excel',
            //             title: 'Users',
            //             text: '<i class="ti ti-file-spreadsheet me-2"></i>Excel',
            //             className: 'dropdown-item',
            //             exportOptions: {
            //                 columns: [2, 3],
            //                 // prevent avatar to be display
            //                 format: {
            //                     body: function body(inner, coldex, rowdex) {
            //                         if (inner.length <= 0) return inner;
            //                         var el = $.parseHTML(inner);
            //                         var result = '';
            //                         $.each(el, function(index, item) {
            //                             if (item.classList.contains(
            //                                     'user-name')) {
            //                                 result = result + item.lastChild
            //                                     .textContent;
            //                             } else result = result + item.innerText;
            //                         });
            //                         return result;
            //                     }
            //                 }
            //             }
            //         }, {
            //             extend: 'pdf',
            //             title: 'Users',
            //             text: '<i class="ti ti-file-text me-2"></i>Pdf',
            //             className: 'dropdown-item',
            //             exportOptions: {
            //                 columns: [2, 3],
            //                 // prevent avatar to be display
            //                 format: {
            //                     body: function body(inner, coldex, rowdex) {
            //                         if (inner.length <= 0) return inner;
            //                         var el = $.parseHTML(inner);
            //                         var result = '';
            //                         $.each(el, function(index, item) {
            //                             if (item.classList.contains(
            //                                     'user-name')) {
            //                                 result = result + item.lastChild
            //                                     .textContent;
            //                             } else result = result + item.innerText;
            //                         });
            //                         return result;
            //                     }
            //                 }
            //             }
            //         }, {
            //             extend: 'copy',
            //             title: 'Users',
            //             text: '<i class="ti ti-copy me-1" ></i>Copy',
            //             className: 'dropdown-item',
            //             exportOptions: {
            //                 columns: [2, 3],
            //                 // prevent avatar to be copy
            //                 format: {
            //                     body: function body(inner, coldex, rowdex) {
            //                         if (inner.length <= 0) return inner;
            //                         var el = $.parseHTML(inner);
            //                         var result = '';
            //                         $.each(el, function(index, item) {
            //                             if (item.classList.contains(
            //                                     'user-name')) {
            //                                 result = result + item.lastChild
            //                                     .textContent;
            //                             } else result = result + item.innerText;
            //                         });
            //                         return result;
            //                     }
            //                 }
            //             }
            //         }]
            //     }, 
            //     // "Add New" button
            //     // {
            //     //     text: '<i class="ti ti-plus me-0 me-sm-1"></i><span class="d-none d-sm-inline-block">Add New</span>',
            //     //     className: 'add-new btn btn-primary',
            //     //     attr: {
            //     //         'data-bs-toggle': 'modal',
            //     //         'data-bs-target': '#add_modal'
            //     //     }
            //     // }
            //     ],
            //     // For responsive popup
            //     responsive: {
            //         details: {
            //             display: $.fn.dataTable.Responsive.display.modal({
            //                 header: function header(row) {
            //                     var data = row.data();
            //                     return 'Details of ' + data['name'];
            //                 }
            //             }),
            //             type: 'column',
            //             renderer: function renderer(api, rowIdx, columns) {
            //                 var data = $.map(columns, function(col, i) {
            //                     return col.title !==
            //                         '' // ? Do not show row in modal popup if title is blank (for check box)
            //                         ?
            //                         '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' +
            //                         col
            //                         .columnIndex + '">' + '<td>' + col.title + ':' + '</td> ' +
            //                         '<td>' + col.data + '</td>' + '</tr>' : '';
            //                 }).join('');
            //                 return data ? $('<table class="table"/><tbody />').append(data) : false;
            //             }
            //         }
            //     }
            // });



            $("#add_form").submit(function(e) {
                e.preventDefault(); // Prevent the default form submission
                $('#loader').show(); // Show the loader

                // Use jQuery AJAX to submit the form data
                $.ajax({
                    url: "{{ url('properties') }}" + "/" + propertyId + "/room-condition",
                    type: "POST",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        $('#loader').hide(); // Hide the loader

                        // Check the response structure
                        if (response.code === "200" && response.status === "success") {
                            // This is a successful operation
                            Swal.fire({
                                icon: 'success',
                                title: 'Operation update successful!',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            }).then(function() {
                                // Reload the window after the user acknowledges the success message
                                window.location.reload();
                            });
                        } else {
                            // Handle other unexpected response structures here
                            Swal.fire({
                                icon: 'error',
                                title: 'An unexpected response occurred!',
                                customClass: {
                                    confirmButton: 'btn btn-danger'
                                }
                            });
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $('#loader').hide(); // Hide the loader

                        // Handle HTTP errors using Swal.fire
                        Swal.fire({
                            icon: 'error',
                            title: 'An unexpected error occurred!',
                            text: 'Please try again later.',
                            customClass: {
                                confirmButton: 'btn btn-danger'
                            }
                        });
                    }
                });
            });



            // reset form when modal is closed without clicking the submit button
            $('#add_modal').on('hidden.bs.modal', function(e) {
                // $('#add_form')[0].reset();
                $('#add_form').trigger("reset");
            });

        });

        function ServingEdit(id) {
            rowid = id;
            // console.log(rowid);

            $.ajax({
                url: "{{ url('properties') }}" + "/" + propertyId + "/room-condition/" + rowid + "/edit",
                type: "GET",
                success: function(response) {
                    // console.log(response[0]);
                    // response = undefined;

                    // Populate the form fields with data
                    $('#room_id').val(response?.room_id ?? rowid);

                    // Loop through 'room condition' data and 'check' radio buttons accordingly
                    $.each(response, function(index, item) {

                        // console.log(item.description);
                        // Check the appropriate radio button based on the 'status' value
                        $(`input[type="radio"][name="status[${item.room_service_id}]"]`).eq(0).prop(
                            'checked', item.status === 'satisfied');
                        $(`input[type="radio"][name="status[${item.room_service_id}]"]`).eq(1).prop(
                            'checked', item.status === 'need_repairing');
                        $(`input[type="radio"][name="status[${item.room_service_id}]"]`).eq(2).prop(
                            'checked', item.status === 'dirty');
                        $(`textarea[name="description[${item.room_service_id}]"]`).val(item
                            .description);

                        // also works
                        // $(`input[type="radio"][name="status[${item.room_service_id}]"]`).first().prop('checked', item.status === 'satisfied');
                        // $(`input[type="radio"][name="status[${item.room_service_id}]"]`).last().prop('checked', item.status === 'need_repairing');

                        // can be used if response from 'edit' method contains the 'RoomService' relationship data (whick contains the Room Service Names)
                        // $('#' + roomService.name + '1').prop('checked', roomService.status === 'satisfied');
                        // $('#' + roomService.name + '2').prop('checked', roomService.status === 'need_repairing');

                        // does not work, because the input type radio with same names are not 'siblings' as they have 'different parents'
                        // $(`input[type="radio"][name="status[${item.room_service_id}]"]:nth-of-type(1)`).prop('checked', item.status === 'satisfied');
                        // $(`input[type="radio"][name="status[${item.room_service_id}]"]:nth-of-type(2)`).prop('checked', item.status === 'need_repairing');
                    });

                    $('#add_modal').modal('show');

                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        }
    </script>
@endsection
