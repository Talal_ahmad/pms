@extends('admin.layouts.master')
@section('title', 'Room Condition')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-2 mb-1"><span class="text-muted fw-light">Room Condition /</span> List</h4>
        <div class="card">
            <div class="card-datatable table-responsive">

                <div class="d-flex justify-content-around">
                    <div class="h5 p-3 m-0 w-auto">
                        House Keeper: <span class="fw-normal text-secondary">{{ $house_keeper->user->name }}</span>
                    </div>
                    <div class="h5 p-3 m-0 w-auto">
                        Floor: <span class="fw-normal text-secondary">{{ $house_keeper->floor->name }}</span>
                    </div>
                </div>
                
                <table class="datatables-users table">
                    <thead class="border-top">
                        <tr>
                            <th class="not_include"></th>
                            <th>Sr.#</th>
                            <th>Room Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <!-- Add New Modal -->
            <div class="modal fade" id="add_modal" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered1 modal-lg modal-simple modal-add-new-cc">
                    <div class="modal-content p-3 p-md-5">
                        <div class="modal-body">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            <div class="text-center mb-4">
                                <h3 class="mb-2">Update Room Condition</h3>
                            </div>
                            <form id="add_form" class="form row g-3">
                                @csrf
                                {{-- <div class="col-6">
                                    <label class="form-label" for="room_service">room_services</label>
                                    <select id="users" name="room_service" class="form-select select2"
                                        data-placeholder="Select User">
                                        <option value=""></option>
                                        @foreach ($room_services as $room_service)
                                            <option value="{{ $room_service->id }}">{{ $room_service->name }}</option>
                                        @endforeach
                                    </select>
                                </div> --}}
                                
                                <input type="hidden" id="room_id" name="room_id">
                                
                                @foreach ($room_services as $room_service)
                                    <div class="col-12 d-flex flex-wrap">
                                        <input type="hidden" name="room_service_id[]" value="{{ $room_service->id }}">
                                
                                        <h4 class="col-6 text-capitalize">
                                            {{ $room_service->name }}
                                        </h4>

                                        <div class="col-2 form-check form-check-inline">
                                            <label class="form-check-label" for="{{ $room_service->name }}1">
                                                Satisfied
                                            </label>
                                            <input class="form-check-input" type="radio" name="status[{{ $room_service->id }}]" id="{{ $room_service->name }}1" value="satisfied" checked>
                                        </div>

                                        <div class="col-3 form-check form-check-inline">
                                            <label class="form-check-label" for="{{ $room_service->name }}2">
                                                Need Repairing
                                            </label>
                                            <input class="form-check-input" type="radio" name="status[{{ $room_service->id }}]" id="{{ $room_service->name }}2" value="need_repairing">
                                        </div>
                                        <div class="col-12">
                                            <label class="" for="{{ $room_service->name }}3">
                                                Description:
                                            </label>
                                            <textarea class="form-control" placeholder="Leave a comment here" id="{{ $room_service->name }}3" name="description[{{ $room_service->id }}]"></textarea>
                                        </div>
                                    </div>
                                    <hr class="mb-0 mt-3 p-0">
                                @endforeach
                            

                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                                    <button type="reset" class="btn btn-label-secondary btn-reset" data-bs-dismiss="modal"
                                        aria-label="Close">
                                        Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Add New Modal -->

        </div>
        <div class="sk-wave sk-primary" id="loader"
            style="display: none; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var propertyId = '{!! request()->route('property')->id !!}';
        $(document).ready(function() {

            // $('.select2').select2();

            // $('#add_modal').modal('show');

            // when a Floor is selected, get its Rooms (not working , and not being used)
            // $('#floors').on('change', function () {
                //     // Get the selected floor id
                //     var selectedValue = $(this).val();

                //     // Make an AJAX call with the selected value
                //     $.ajax({
                //         url: "{{ url('properties') }}" + "/" + propertyId + "/housekeeper/getrooms",
                //         type: 'GET',
                //         data: { 
                //                 floor_id: selectedValue,
                //                 _token: "{{ csrf_token() }}"
                //               },
                //         success: function(response) {

                //             console.log('response is:' + response);

                //             // enable the Rooms Select2
                //             $('#rooms').prop('disabled', false);

                //             // Clear existing options
                //             $('#rooms').empty();

                //             // Set data-placeholder attribute
                //             $('#rooms').attr('data-placeholder', 'Select Rooms');

                //             // Add options from server data
                //             $('#rooms').append('<option value=""></option>'); // Empty option

                //             $.each(response, function (index, room) {
                //                 $('#rooms').append('<option value="' + room.id + '">' + room.name + '</option>');
                //             });

                //             // Trigger change to update Select2
                //             $('#rooms').trigger('change');

                //         },
                //         error: function(xhr, status, error) {
                //             console.error(xhr.responseText);
                //         }
                //     });
            // });
            
            var url = "{{ route('room-condition.index', ['property' => ':propertyId']) }}";
            url = url.replace(':propertyId', propertyId);
            dataTable = $('.datatables-users').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [
                    // columns according to JSON
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'action'
                    }
                ],
                columnDefs: [{
                    // For Responsive
                    className: 'control',
                    searchable: false,
                    orderable: false,
                    responsivePriority: 2,
                    targets: 0,
                    render: function render(data, type, full, meta) {
                        // console.log(full);
                        return '';
                    }
                }, {
                    // Actions
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function render(data, type, full, meta) {
                        // console.log(data);
                        // console.log(full.id);
                        return '<div class="d-inline-block text-nowrap">' +
                            '<a href="javascript:;" class="text-body" onclick=ServingEdit(' +
                            full.id + ')>' + '<i class=\"ti ti-edit\"></i>' +
                            '</a>';
                    }
                }],
                order: [
                    [2, 'desc']
                ],
                dom: '<"row mx-2"' + '<"col-md-2"<"me-3"l>>' +
                    '<"col-md-10"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"fB>>' +
                    '>t' + '<"row mx-2"' + '<"col-sm-12 col-md-6"i>' + '<"col-sm-12 col-md-6"p>' + '>',
                language: {
                    sLengthMenu: '_MENU_',
                    search: '',
                    searchPlaceholder: 'Search..'
                },
                // Buttons with Dropdown
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-label-primary dropdown-toggle mx-3',
                    text: '<i class="ti ti-logout rotate-n90 me-2"></i>Export',
                    buttons: [{
                        extend: 'print',
                        title: 'Users',
                        text: '<i class="ti ti-printer me-2" ></i>Print',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be print
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList !== undefined && item
                                            .classList.contains('user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        },
                        customize: function customize(win) {
                            //customize print view for dark
                            $(win.document.body).css('color', config.colors
                                    .headingColor)
                                .css('border-color', config.colors.borderColor).css(
                                    'background-color', config.colors.body);
                            $(win.document.body).find('table').addClass('compact').css(
                                    'color', 'inherit').css('border-color', 'inherit')
                                .css(
                                    'background-color', 'inherit');
                        }
                    }, {
                        extend: 'csv',
                        title: 'Users',
                        text: '<i class="ti ti-file-text me-2" ></i>Csv',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be print
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'excel',
                        title: 'Users',
                        text: '<i class="ti ti-file-spreadsheet me-2"></i>Excel',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be display
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'pdf',
                        title: 'Users',
                        text: '<i class="ti ti-file-text me-2"></i>Pdf',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be display
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'copy',
                        title: 'Users',
                        text: '<i class="ti ti-copy me-1" ></i>Copy',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be copy
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                                'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }]
                }, 
                // "Add New" button
                // {
                //     text: '<i class="ti ti-plus me-0 me-sm-1"></i><span class="d-none d-sm-inline-block">Add New</span>',
                //     className: 'add-new btn btn-primary',
                //     attr: {
                //         'data-bs-toggle': 'modal',
                //         'data-bs-target': '#add_modal'
                //     }
                // }
                ],
                // For responsive popup
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function header(row) {
                                var data = row.data();
                                return 'Details of ' + data['name'];
                            }
                        }),
                        type: 'column',
                        renderer: function renderer(api, rowIdx, columns) {
                            var data = $.map(columns, function(col, i) {
                                return col.title !==
                                    '' // ? Do not show row in modal popup if title is blank (for check box)
                                    ?
                                    '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' +
                                    col
                                    .columnIndex + '">' + '<td>' + col.title + ':' + '</td> ' +
                                    '<td>' + col.data + '</td>' + '</tr>' : '';
                            }).join('');
                            return data ? $('<table class="table"/><tbody />').append(data) : false;
                        }
                    }
                }
            });



            $("#add_form").submit(function(e) {
                $('#loader').show();
                e.preventDefault();
                // console.log(...new FormData(this));
                // return;

                $.ajax({
                    url: "{{ url('properties') }}" + "/" + propertyId + "/room-condition",
                    type: "POST",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function success(response) {
                        // sweetalert
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Swal.fire({
                                    title: value,
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            });
                        } else if (response.error_message) {
                            swal.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form')[0].reset();
                            dataTable.ajax.reload();
                            $('#loader').hide();
                            $('#add_modal').modal('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'Room Condition has been Updated Successfully!',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        }
                    },
                });
            });

        });

        function ServingEdit(id) {
            rowid = id;
            // console.log(rowid);

            $.ajax({
                url: "{{ url('properties') }}" + "/" + propertyId + "/room-condition/" + rowid + "/edit",
                type: "GET",
                success: function(response) {
                    // console.log(response[0]);
                    // response = undefined;

                    // Populate the form fields with data
                    $('#room_id').val(response?.room_id ?? rowid);

                    // Loop through 'room condition' data and 'check' radio buttons accordingly
                    $.each(response, function(index, item) {

                        console.log(item.description);
                        // Check the appropriate radio button based on the 'status' value
                        $(`input[type="radio"][name="status[${item.room_service_id}]"]`).eq(0).prop('checked', item.status === 'satisfied');
                        $(`input[type="radio"][name="status[${item.room_service_id}]"]`).eq(1).prop('checked', item.status === 'need_repairing');
                        $(`textarea[name="description[${item.room_service_id}]"]`).val(item.description);
                        
                        // also works
                        // $(`input[type="radio"][name="status[${item.room_service_id}]"]`).first().prop('checked', item.status === 'satisfied');
                        // $(`input[type="radio"][name="status[${item.room_service_id}]"]`).last().prop('checked', item.status === 'need_repairing');
                        
                        // can be used if response from 'edit' method contains the 'RoomService' relationship data (whick contains the Room Service Names)
                        // $('#' + roomService.name + '1').prop('checked', roomService.status === 'satisfied');
                        // $('#' + roomService.name + '2').prop('checked', roomService.status === 'need_repairing');

                        // does not work, because the input type radio with same names are not 'siblings' as they have 'different parents'
                        // $(`input[type="radio"][name="status[${item.room_service_id}]"]:nth-of-type(1)`).prop('checked', item.status === 'satisfied');
                        // $(`input[type="radio"][name="status[${item.room_service_id}]"]:nth-of-type(2)`).prop('checked', item.status === 'need_repairing');
                    });
                    
                    $('#add_modal').modal('show');

                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        }

        // function deleteServing(id) {
        //     rowid = id;

        //     // console.log(rowid);

        //     $.confirm({
        //         icon: 'far fa-question-circle',
        //         title: 'Confirm!',
        //         content: 'Are you sure you want to delete!',
        //         type: 'orange',
        //         typeAnimated: true,
        //         buttons: {
        //             Confirm: {
        //                 text: 'Confirm',
        //                 btnClass: 'btn-orange',
        //                 action: function() {
        //                     $.ajax({
        //                         url: "{{ url('properties') }}" + "/" + propertyId + "/room-condition/" + rowid,
        //                         type: "DELETE",
        //                         data: {
        //                             _token: "{{ csrf_token() }}"
        //                         },
        //                         success: function(response) {
        //                             if (response.error_message) {
        //                                 Toast.fire({
        //                                     icon: 'error',
        //                                     title: 'An error has been occured! Please Contact Administrator.'
        //                                 })
        //                             } else if (response.code == 300) {
        //                                 $.alert({
        //                                     icon: 'far fa-times-circle',
        //                                     title: 'Oops!',
        //                                     content: response.message,
        //                                     type: 'red',
        //                                     buttons: {
        //                                         Okay: {
        //                                             text: 'Okay',
        //                                             btnClass: 'btn-red',
        //                                         }
        //                                     }
        //                                 });
        //                             } else {
        //                                 dataTable.ajax.reload();
        //                                 Swal.fire({
        //                                     icon: 'success',
        //                                     title: 'House Keeper has been Deleted Successfully!',
        //                                     customClass: {
        //                                         confirmButton: 'btn btn-success'
        //                                     }
        //                                 });
        //                             }
        //                         }
        //                     });
        //                 }
        //             },
        //             cancel: function() {
        //                 $.alert('Canceled!');
        //             },
        //         }
        //     });
        // }
    </script>
@endsection
