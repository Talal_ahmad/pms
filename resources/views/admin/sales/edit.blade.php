@extends('admin.layouts.master')
@section('title', 'POS')
@section('styles')
    @vite(['resources/js/app.js'])
@endsection
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y" id="app">
        <pos-edit-component :property={{ getProperty() }} :order-id={{ $order }}></pos-edit-component>
    </div>
@endsection
