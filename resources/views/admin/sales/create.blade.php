@extends('admin.layouts.master')
@section('title', 'POS')
@section('styles')
    @vite(['resources/js/app.js'])
@endsection
@section('content')
    <div class="container-xxl flex-grow-1 " id="app">
        <pos-create-component :property={{ getProperty() }}></pos-create-component>
    </div>
@endsection
