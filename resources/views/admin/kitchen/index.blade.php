@extends('admin.layouts.master')
@section('title', 'Menu Items')
@section('styles')
    @vite(['resources/js/app.js'])
@endsection
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y" id="app">
        <kitchen-component :property={{ getProperty() }}></kitchen-component>
    </div>
@endsection



{{-- 



@extends('admin.layouts.master')
@section('title', 'Menu Items')
@section('styles')
    <style>
        .select2-container {
            z-index: 999999;
        }
    </style>
@endsection
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        - - - - - - - - --- <section class="rounded" style="background-color: #D9D9D9; "> --
        <div class="container ">
            <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-4">
                @foreach ($orders as $order)
                    <div class="col p-0">
                        <div class="card p-0 rounded-4 m-4 shadow-lg" style="border-radius: 10px;">
                            <div class=" p-2 "
                                style="background-color: #7367F0; border-top-right-radius: 10px;border-top-left-radius:10px">
                                <div class="d-flex justify-content-between text-white p-2"
                                    style="font-family: poppins; font-weight: 500; font-size: 16px;">
                                    <div># {{ $order->order_number }}</div>
                                    <div><i class="fa-solid fa-dice-d20"></i>
                                        {{ $order->type }} </div>
                                    -- <div><i class="fa-regular fa-user"></i> SAAD REHMAN</div> --
                                </div>
                                <div class="text-white p-2"
                                    style="font-family: poppins; font-weight: 500; font-size: 16px;">
                                    -- <div><i class="fa-solid fa-people-group"></i> Guest 4</div> --
                                    -- <div> <img src="{{ asset('/Images/fi-sr-presentation.png') }}" alt=""> T6</div> --
                                    <div class="text-center"><img src="Vector.png" alt=""> Created at :
                                        {{ $order->created_at }}</div>
                                </div>
                            </div>
                            <div class=" p-2 " style="background-color: #F7F7F7;">
                                <div class="d-flex justify-content-between text-black"
                                    style="font-family: poppins; font-weight: 500; font-size: 14px;">
                                    <div style="margin-left: 20px; font-weight: 600;">Food Item</div>
                                    <div style="font-weight: 600;">Quantity</div>
                                </div>
                            </div>
                            <div class="card-body pt-0">
                                <div class="mb-5 ">
                                    @foreach ($order->orderDetails as $orderDetail)
                                        <div class="d-flex justify-content-between text-black "
                                            style="font-family: Montserrat; font-weight: 500; font-size: 13px; ">
                                            <div>{{ $orderDetail->menuItem->name }} -
                                                {{ $orderDetail->menuServing ? $orderDetail->menuServing->name : 'No Serving' }}
                                            </div>
                                            <div style="margin-right: 20px;">{{ $orderDetail->quantity }}</div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="d-flex justify-content-between ">
                                    <div style="color: #737B7B; font-family: poppins;font-size: 14px;">Note <img
                                            class="mb-3" src="Group 1000004525.png" alt=""></div>
                                    <div>
                                        <button type="button" class="btn btn-success border-0 mark-ready-btn"
                                            data-order-id="{{ $order->id }}"
                                            style="background-color: #7367F0; font-family: Montserrat; font-weight: 400; font-size: 13px;">
                                            Mark Ready
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
        -- </section> --

    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $('.mark-ready-btn').on('click', function() {
                var orderId = $(this).data('order-id');
                var propertyId = {!! request()->route('property')->id !!};

                $.ajax({
                    // "{{ url('/properties') }}" + '/' + propertyId + '/sale-report'
                    url: "{{ url('/properties') }}" + '/' + propertyId + '/mark-order-ready/' +
                        orderId,
                    type: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}', // Include CSRF token if needed
                    },
                    success: function success(response) {
                        // sweetalert
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Swal.fire({
                                    title: value,
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            });
                        } else if (response.error_message) {
                            swal.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            location.reload();
                            $('#loader').hide();
                            Swal.fire({
                                icon: 'success',
                                title: 'Order has been Complete Successfully!',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        }
                    },
                });
            });
        });
    </script>
@endsection --}}
