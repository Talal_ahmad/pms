@extends('admin.layouts.master')
@section('title', 'Floors')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-2 mb-1"><span class="text-muted fw-light">Floors /</span> Edit</h4>

        <div class="col-12 mb-3 pe-4 text-end">
            <button type="button" onclick="redirect()" class="btn btn-primary ms-auto">Back</button>
        </div>

        <div class="card">

            {{-- Edit Modal start --}}
            {{-- <div class="modal fade" id="edit_modal" tabindex="-1" aria-hidden="true"> --}}
            {{-- <div class="modal-dialog modal-dialog-centered1 modal-simple modal-lg modal-add-new-cc"> --}}
            <div class="p-3 p-md-5 z-3">
                {{-- <div class=""> --}}
                {{-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> --}}
                {{-- <div class="text-center mb-4">
                              <h3 class="mb-2">Edit Floor</h3>
                          </div> --}}
                @php
                    $propertyType = request()->route('property')->propertyType->name;
                @endphp
                <form id="update_form" class="form row g-3" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <div class="col-12">

                        <div class="mb-3 col-6">
                            <label class="form-label" for="edit-floor-name">Floor Name</label>
                            <input type="text" id="edit-floor-name" class="form-control" name="name"
                                value="{{ $floor->name }}" placeholder="Enter Floor Name" />
                        </div>

                        @isset($propertyType)

                            {{-- "(rooms / tables)" repeater container --}}
                            <div class="edit-form-repeater">

                                @if ($propertyType === 'Hotel')
                                    {{-- "rooms" repeater (list) --}}
                                    <div data-repeater-list="rooms">

                                        @foreach ($floor->rooms as $room)
                                            {{-- "rooms" repeater (item) --}}
                                            <div data-repeater-item>
                                                <div class="row justify-content-between">

                                                    <div class="w-0 h-0 invisible">
                                                        <input type="hidden" name="id" value="{{ $room->id }}"
                                                            class="w-0 h-0 edit-room-id" />
                                                    </div>

                                                    <div class="mb-3 col-6">
                                                        <label class="form-label">Room Type</label>
                                                        <select name="room_type_id" class="form-select select2 edit-room-type"
                                                            data-placeholder="Select Room Type" required>

                                                            <option value=""></option>
                                                            @isset($roomTypes)
                                                                @foreach ($roomTypes as $roomType)
                                                                    <option value="{{ $roomType->id }}"
                                                                        @selected($roomType->id === $room->room_type_id)>{{ $roomType->name }}
                                                                    </option>
                                                                @endforeach
                                                            @endisset
                                                        </select>
                                                    </div>
                                                    <div class="mb-3 col-4">
                                                        <label class="form-label">Room Name</label>
                                                        <input type="text" value="{{ $room->name }}" name="name"
                                                            class="form-control edit-room-name" placeholder="Enter Room Name" />
                                                    </div>

                                                    <div class="mb-3 col-6">
                                                        <label class="form-label">Room Capacity</label>
                                                        <input type="number" name="capacity" class="form-control"
                                                            value="1" placeholder="Enter Room Capacity" min="1"
                                                            max="127" maxlength="3" value="{{ $room->capacity }}" />
                                                    </div>
                                                    <div class="mb-3 col-6">
                                                        <label class="form-label">Room Price</label>
                                                        <input type="number" name="price" class="form-control" value="1"
                                                            placeholder="Enter Room Price" value="{{ $room->price }}" />
                                                    </div>

                                                    {{-- only for testing --}}
                                                    {{-- <div class="mb-3 col-6">
                                                        <label class="form-label">Room Images</label>
                                                        <div>
                                                            {{ $room->cover_image }}
                                                            {{ asset('storage-images/rooms/'. $room->cover_image) }}
                                                            {{ $room->other_images }}
                                                        </div>
                                                    </div> --}}

                                                    {{-- for now we are now not using images modal --}}
                                                    {{-- Images Modal start --}}
                                                    {{-- <div class="modal fade" id="image-modal" tabindex="-1" aria-hidden="true"> --}}
                                                    {{-- <div class="modal fade" tabindex="-1" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered1 modal-simple modal-lg">
                                                            <div class="modal-content p-3 p-md-5">
                                                                <div class="modal-body">
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                    <div class="text-center mb-4">
                                                                        <h3 class="mb-2">Floor's Present Images</h3>
                                                                    </div>

                                                                    <div class="col-12 row g-3">
                                                                        <div>
                                                                            <img src="{{ asset('storage-images/rooms/'. $room->cover_image) }}" alt="" width="200">
                                                                        </div>
                                                                        
                                                                        <div>
                                                                            @foreach (explode('|', $room->other_images) as $other_image)
                                                                        
                                                                                <img src="{{ asset('storage-images/rooms/'. $other_image) }}" alt="" width="200">
                                                                        
                                                                            @endforeach
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> --}}
                                                    {{-- Images Modal end --}}
  

                                                    <div class="mb-3 col-10">
                                                        <label class="form-label">Room Cover Image</label>
                                                        <input type="file" name="cover_image" class="form-control"
                                                            placeholder="Select Room Cover Image"
                                                            accept=".jpg, .jpeg, .png, .bmp, .gif, .svg, .webp" />
                                                    </div>
                                                    

                                                    <div class="mb-3 col-10">
                                                        <label class="form-label">Room Other Images</label>
                                                        {{-- <input type="file" name="other_images[]" class="form-control" --}}
                                                        {{-- changing from array to simple because the payload is sent in wrong format --}}
                                                        <input type="file" name="other_images" class="form-control"
                                                            placeholder="Select Room Other Images"
                                                            accept=".jpg, .jpeg, .png, .bmp, .gif, .svg, .webp" multiple />
                                                    </div>

                                                    <div class="mb-3 col-lg-2 col-xl-2 col-12">
                                                        <button type="button" class="btn btn-label-danger mt-4 col-12"
                                                            title="Remove Room" data-repeater-delete>
                                                            <i class="ti ti-trash ti-xs me-1"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>

                                    <div class="mb-0">
                                        <button type="button" class="btn btn-primary" data-repeater-create>
                                            <i class="ti ti-plus me-1"></i>
                                            <span class="align-middle">Add Room</span>
                                        </button>
                                    </div>
                                @elseif ($propertyType === 'Restaurant')
                                    {{-- "tables" repeater (list) --}}
                                    <div data-repeater-list="tables">

                                        @foreach ($floor->tables as $table)
                                            {{-- "tables" repeater (item) --}}
                                            <div data-repeater-item>
                                                <div class="row justify-content-between">
                                                    <div class="w-0 h-0 invisible">
                                                        <input type="hidden" name="id" value="{{ $table->id }}"
                                                            class="w-0 h-0 edit-table-id" />
                                                    </div>
                                                    <div class="mb-3 col-6">
                                                        <label class="form-label">Table Name</label>
                                                        <input type="text" class="form-control edit-table-name"
                                                            name="name" value="{{ $table->name }}"
                                                            placeholder="Enter Table Name" />
                                                    </div>
                                                    <div class="mb-3 col-4">
                                                        <label class="form-label">Seating Capacity</label>
                                                        <input type="number" class="form-control edit-seating-capacity"
                                                            name="seating_capacity" value="{{ $table->seating_capacity }}"
                                                            placeholder="Enter Table Capacity" />
                                                    </div>
                                                    <div class="mb-3 col-lg-2 col-xl-2 col-12">
                                                        <button type="button" class="btn btn-label-danger mt-4 col-12"
                                                            title="Remove Table" data-repeater-delete>
                                                            <i class="ti ti-trash ti-xs me-1"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>

                                    <div class="mb-0">
                                        <button type="button" class="btn btn-primary" data-repeater-create>
                                            <i class="ti ti-plus me-1"></i>
                                            <span class="align-middle">Add Table</span>
                                        </button>
                                    </div>
                                @endif

                            </div>
                        @else
                            <div>Server Error</div>
                        @endisset

                        <hr />

                    </div>
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-primary me-sm-3 me-1">Update</button>
                    </div>
                </form>
                {{-- </div> --}}
            </div>
            {{-- </div> --}}
            {{-- </div> --}}
            {{-- Edit Modal end --}}

        </div>
        <div class="sk-wave sk-primary" id="loader"
            style="display: none; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // var property = '{!! request()->route('property')->propertyType->name !!}';
        // console.log(property);
        // var floorId = '{!! request()->route('floor')->id !!}';
        // var floorId = "{{ $floor->id }}";
        // console.log(floorId);

        var propertyId = '{!! request()->route('property')->id !!}';

        // redirect back
        function redirect() {
            window.location.replace(`{{ url('properties') }}/${propertyId}/floors`);
        }

        $(document).ready(function() {

            // preventing users from entering value below 1
            $('input[type="number"]').on('input', function() {
                if ($(this).val() < 1) {
                    $(this).val(1);
                }
            });


            // when user clicks the input type="file", open the Images Modal
            // $('input[type="file"]').on("click", function(e) {
            // $('input[name="other_images[]"]').on("click", function(e) {

            //     e.preventDefault();
            //     e.stopPropagation();
                    
            //     // Ensure that the modal is initialized and shown after a small delay in ms
            //     // setTimeout(() => {
            //     //     // $(this).closest('div[data-repeater-item]').find('div.modal').modal('show');
            //     //     // $('#image-modal').modal('show');
            //     // }, 1000);
            //     $(this).closest('div[data-repeater-item]').find('div.modal').modal('show');
            // });


            

            // Repeater Configuration for "update_form"
            $('.edit-form-repeater').repeater({

                show: function() {
                    var fromControl = $(this).find('.form-control, .form-select');
                    var formLabel = $(this).find('.form-label');

                    $(this).slideDown();
                    $(this).find('select').next('.select2-container').remove();

                    $('[data-repeater-list="rooms"]').find('.select2').select2();
                    $('[data-repeater-list="tables"]').find('.select2').select2();

                    // preventing users from entering value below 1 (in newly added repeater-item)
                    $('input[type="number"]').on('input', function() {
                        if ($(this).val() < 1) {
                            $(this).val(1);
                        }
                    });
                },
                hide: function(e) {
                    confirm('Are you sure you want to delete this element?') && $(this).slideUp(e);
                }
            });

            $("#update_form").submit(function(e) {
                $('#loader').show();
                e.preventDefault();

                $.ajax({
                    url: "{{ url('properties') }}" + "/" + propertyId + "/floors/" +
                        "{{ $floor->id }}",
                    type: 'POST',
                    data: new FormData(this),
                    // data: formData,
                    contentType: false,
                    processData: false,
                    success: function success(response) {
                        // sweetalert
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Swal.fire({
                                    title: value,
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            });
                        } else if (response.error_message) {
                            swal.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#loader').hide();
                            Swal.fire({
                                icon: 'success',
                                title: 'Floor has been Updated Successfully!',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                            redirect();
                        }
                    },
                    error:  function(jqXHR, textStatus, errorThrown) {
                        $.each(jqXHR.responseJSON.errors, function(index, value) {
                            Swal.fire({
                                title: value,
                                icon: 'error',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        });
                    } 
                });

            });

        });
    </script>
@endsection
