@extends('admin.layouts.master')
@section('title', 'Dashboard')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <!-- Generated Leads -->
            <div class="col-xl-4 mb-4 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div class="d-flex flex-column">
                                <div class="card-title mb-auto">
                                    <h5 class="mb-1 text-nowrap">Gender Wise</h5>
                                    {{-- <small>Monthly Report</small> --}}
                                </div>
                                {{-- <div class="chart-statistics">
                                    <h3 class="card-title mb-1">4,350</h3>
                                    <small class="text-success text-nowrap fw-medium"><i class='ti ti-chevron-up me-1'></i>
                                        15.8%</small>
                                </div> --}}
                            </div>
                            {{-- <div id="generatedLeadsChart"></div> --}}
                            <div id="donutChart"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Generated Leads -->

            <!-- Statistics -->
            <div class="col-xl-8 mb-4 col-lg-7 col-12">
                <div class="card h-100">
                    <div class="card-header">
                        <div class="d-flex justify-content-between mb-3">
                            <h5 class="card-title mb-0">In House State</h5>
                            <small class="text-muted">Updated 1 month ago</small>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row gy-3 mt-3">
                            <div class="col-md-3 col-6">
                                <div class="d-flex align-items-center">
                                    <div class="badge rounded-pill bg-label-primary me-3 p-2"><i
                                            class="fa-solid fa-users ti-sm"></i></div>
                                    <div class="card-info">
                                        <h5 class="mb-0">{{ $customer_count }}</h5>
                                        <small>Total Guest</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="d-flex align-items-center">
                                    <div class="badge rounded-pill bg-label-info me-3 p-2"><i
                                            class="ti ti-credit-card ti-sm"></i>
                                    </div>
                                    <div class="card-info">
                                        <h5 class="mb-0">{{ $room_count }}</h5>
                                        <small>Total Room</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="d-flex align-items-center">
                                    <div class="badge rounded-pill bg-label-danger me-3 p-2"><i
                                            class="fa-solid fa-bed ti-sm"></i></div>
                                    <div class="card-info">
                                        <h5 class="mb-0">{{ $booking_count }}</h5>
                                        <small>Total Booking</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="d-flex align-items-center">
                                    <div class="badge rounded-pill bg-label-success me-3 p-2"><i
                                            class="fa-solid fa-plane-departure ti-sm"></i></div>
                                    <div class="card-info">
                                        <h5 class="mb-0">{{ $checkIn_count }}</h5>
                                        <small>Total Check-In</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Statistics -->
            <!-- Earning Reports -->
            {{-- <div class="col-xl-6 col-md-6 mb-4">
                <div class="card h-100">
                    <div class="card-header d-flex justify-content-between">
                        <div class="card-title mb-0">
                            <h5 class="m-0 me-2">Earning Reports</h5>
                            <small class="text-muted">Weekly Earnings Overview</small>
                        </div>
                        <div class="dropdown">
                            <button class="btn p-0" type="button" id="earningReports" data-bs-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="ti ti-dots-vertical ti-sm text-muted"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-end" aria-labelledby="earningReports">
                                <a class="dropdown-item" href="javascript:void(0);">Download</a>
                                <a class="dropdown-item" href="javascript:void(0);">Refresh</a>
                                <a class="dropdown-item" href="javascript:void(0);">Share</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pb-0">
                        <ul class="p-0 m-0">
                            <li class="d-flex mb-3">
                                <div class="avatar flex-shrink-0 me-3">
                                    <span class="avatar-initial rounded bg-label-primary"><i
                                            class='ti ti-chart-pie-2 ti-sm'></i></span>
                                </div>
                                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <h6 class="mb-0">Net Profit</h6>
                                        <small class="text-muted">12.4k Sales</small>
                                    </div>
                                    <div class="user-progress d-flex align-items-center gap-3">
                                        <small>$1,619</small>
                                        <div class="d-flex align-items-center gap-1">
                                            <i class='ti ti-chevron-up text-success'></i>
                                            <small class="text-muted">18.6%</small>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex mb-3">
                                <div class="avatar flex-shrink-0 me-3">
                                    <span class="avatar-initial rounded bg-label-success"><i
                                            class='ti ti-currency-dollar ti-sm'></i></span>
                                </div>
                                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <h6 class="mb-0">Total Income</h6>
                                        <small class="text-muted">Sales, Affiliation</small>
                                    </div>
                                    <div class="user-progress d-flex align-items-center gap-3">
                                        <small>$3,571</small>
                                        <div class="d-flex align-items-center gap-1">
                                            <i class='ti ti-chevron-up text-success'></i>
                                            <small class="text-muted">39.6%</small>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex mb-3">
                                <div class="avatar flex-shrink-0 me-3">
                                    <span class="avatar-initial rounded bg-label-secondary"><i
                                            class='ti ti-credit-card ti-sm'></i></span>
                                </div>
                                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <h6 class="mb-0">Total Expenses</h6>
                                        <small class="text-muted">ADVT, Marketing</small>
                                    </div>
                                    <div class="user-progress d-flex align-items-center gap-3">
                                        <small>$430</small>
                                        <div class="d-flex align-items-center gap-1">
                                            <i class='ti ti-chevron-up text-success'></i>
                                            <small class="text-muted">52.8%</small>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div id="reportBarChart"></div>
                    </div>
                </div>
            </div> --}}

            <div class="col-xl-6 col-md-6 mb-4">
                <div class="card h-100">
                    <div class="card-header d-flex justify-content-between">
                        <div class="card-title m-0 me-2">
                            <h5 class="m-0 me-2">Available Types States</h5>
                            <small class="text-muted">Room Types Available Chart</small>
                        </div>
                    </div>
                    <div class="card-body">
                        <ul class="p-0 m-0">
                            @foreach ($roomTypeCounts as $roomCount)
                                @php
                                    $totalRooms = $roomCount->total_rooms;
                                    $availableRooms = $roomCount->available_rooms;
                                    // Calculate the average booking rate
                                    $avgAvailableRate =
                                        $totalRooms > 0 ? round(($availableRooms / $totalRooms) * 100) : 0;
                                @endphp
                                <li class="d-flex mb-4 pb-1 align-items-center">
                                    <div class="d-flex w-100 align-items-center gap-2">
                                        <div class="d-flex justify-content-between flex-grow-1 flex-wrap">
                                            <div>
                                                <h6 class="mb-0">{{ $roomCount->room_type }}</h6>
                                            </div>

                                            <div class="user-progress d-flex align-items-center gap-2">
                                                <h6 class="mb-0">{{ $avgAvailableRate }}%</h6>
                                            </div>
                                        </div>
                                        <div class="chart-progress" data-color="info" data-series="{{ $avgAvailableRate }}">
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <!--/ Earning Reports -->
            <!-- Transactions -->
            <div class="col-md-6 col-xl-6 mb-4">
                <div class="card h-100">
                    <div class="card-header d-flex justify-content-between">
                        <div class="card-title m-0 me-2">
                            <h5 class="m-0 me-2">House Keeping Stats</h5>
                            {{-- <small class="text-muted">Total 58 Transactions done in this Month</small> --}}
                        </div>
                        {{-- <div class="dropdown">
                            <button class="btn p-0" type="button" id="transactionID" data-bs-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="ti ti-dots-vertical ti-sm text-muted"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-end" aria-labelledby="transactionID">
                                <a class="dropdown-item" href="javascript:void(0);">Last 28 Days</a>
                                <a class="dropdown-item" href="javascript:void(0);">Last Month</a>
                                <a class="dropdown-item" href="javascript:void(0);">Last Year</a>
                            </div>
                        </div> --}}
                    </div>
                    <div class="card-body">
                        <ul class="p-0 m-0">
                            {{-- <li class="d-flex mb-3 pb-1 align-items-center">
                                <div class="badge bg-label-primary me-3 rounded p-2">
                                    <i class="fa-solid fa-hand-holding-droplet ti-sm"></i>
                                </div>
                                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <h6 class="mb-0">Sanitary</h6>
                                        <small class="text-muted d-block">Starbucks</small>
                                    </div>
                                    <div class="user-progress d-flex align-items-center gap-1">
                                        <h6 class="mb-0 text-danger">-$75</h6>
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex mb-3 pb-1 align-items-center">
                                <div class="badge bg-label-success rounded me-3 p-2">
                                    <i class="fa-solid fa-plug ti-sm"></i>
                                </div>
                                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <h6 class="mb-0">Electronics</h6>
                                        <small class="text-muted d-block">Add Money</small>
                                    </div>
                                    <div class="user-progress d-flex align-items-center gap-1">
                                        <h6 class="mb-0 text-success">+$480</h6>
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex mb-3 pb-1 align-items-center">
                                <div class="badge bg-label-danger rounded me-3 p-2">
                                    <i class="fa-solid fa-person-digging ti-sm"></i>
                                </div>
                                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <h6 class="mb-0">Construction</h6>
                                        <small class="text-muted d-block mb-1">Client Payment</small>
                                    </div>
                                    <div class="user-progress d-flex align-items-center gap-1">
                                        <h6 class="mb-0 text-success">+$268</h6>
                                    </div>
                                </div>
                            </li> --}}
                            {{-- <li class="d-flex mb-3 pb-1 align-items-center">
                                <div class="badge bg-label-secondary me-3 rounded p-2">
                                    <i class="ti ti-credit-card ti-sm"></i>
                                </div>
                                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <h6 class="mb-0">Furniture</h6>
                                        <small class="text-muted d-block mb-1">Ordered iPhone 13</small>
                                    </div>
                                    <div class="user-progress d-flex align-items-center gap-1">
                                        <h6 class="mb-0 text-danger">-$699</h6>
                                    </div>
                                </div>
                            </li> --}}
                            <li class="d-flex mb-3 pb-1 align-items-center">
                                <div class="badge bg-label-info me-3 rounded p-2">
                                    <i class="fa-solid fa-brush  ti-sm"></i>
                                </div>
                                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <h6 class="mb-0">Repair</h6>
                                        {{-- <small class="text-muted d-block mb-1">Refund</small> --}}
                                    </div>
                                    <div class="user-progress d-flex align-items-center gap-1">
                                        <h6 class="mb-0 text-success">{{ $house_keeper_repair_count }}</h6>
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex mb-3 pb-1 align-items-center">
                                <div class="badge bg-label-danger me-3 rounded p-2">
                                    <i class="ti ti-brand-paypal ti-sm"></i>
                                </div>
                                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <h6 class="mb-0">Dirty</h6>
                                        {{-- <small class="text-muted d-block mb-1">Client Payment</small> --}}
                                    </div>
                                    <div class="user-progress d-flex align-items-center gap-1">
                                        <h6 class="mb-0 text-danger">{{ $house_keeper_dirty->dirty_count }}</h6>
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex align-items-center">
                                <div class="badge bg-label-success me-3 rounded p-2">
                                    <i class="ti ti-browser-check ti-sm"></i>
                                </div>
                                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <h6 class="mb-0">Clean</h6>
                                        {{-- <small class="text-muted d-block mb-1">Pay Office Rent</small> --}}
                                    </div>
                                    <div class="user-progress d-flex align-items-center gap-1">
                                        <h6 class="mb-0 text-success">{{ $house_keeper_dirty->satisfied_count }}</h6>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/ Transactions -->
            <!-- Active Projects -->
            <div class="col-xl-8 col-md-6 mb-4">
                <div class="card h-100">
                    <div class="card-header d-flex justify-content-between">
                        <div class="card-title mb-0">
                            <h5 class="mb-0">Selected Room Type</h5>
                            <small class="text-muted">Selected Package Type</small>
                        </div>
                    </div>
                    <div class="card-body">
                        <ul class="p-0 m-0">
                            @foreach ($roomTypeCounts as $roomCount)
                                @php
                                    $totalRooms = $roomCount->total_rooms;
                                    $bookedRooms = $roomCount->booked_rooms;
                                    // Calculate the average booking rate
                                    $avgBookingRate = $totalRooms > 0 ? round(($bookedRooms / $totalRooms) * 100) : 0;
                                @endphp
                                <li class="mb-3 pb-1 d-flex">
                                    <div class="d-flex w-50 align-items-center me-3">
                                        <div>
                                            <h6 class="mb-0">{{ $roomCount->room_type }}</h6>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-grow-1 align-items-center">
                                        <div class="progress w-100 me-3" style="height:8px;">
                                            <div class="progress-bar bg-danger" role="progressbar"
                                                style="width: {{ $avgBookingRate }}%"
                                                aria-valuenow="{{ $avgBookingRate }}" aria-valuemin="0"
                                                aria-valuemax="100">
                                            </div>
                                        </div>
                                        <span class="text-muted">{{ $avgBookingRate }}%</span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                </div>
            </div>
            <div class="col-12 col-xl-4 mb-4 col-md-6">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h5 class="mb-0 card-title">Total Booking</h5>
                    </div>
                    <div class="card-body">
                        <div class="d-flex align-items-start">
                            <div class="badge rounded bg-label-warning p-2 me-3 rounded">
                                <i class="fa-solid fa-bed ti-sm"></i>
                            </div>
                            <div class="d-flex justify-content-between w-100 gap-2 align-items-center">
                                <div class="me-2">
                                    <h6 class="mb-0">{{ $total_booking }}</h6>
                                    <small class="text-muted">Total Booking</small>
                                </div>
                            </div>
                        </div>
                        <div id="projectStatusChart"></div>
                        <div class="d-flex justify-content-between mb-3">
                            <h6 class="mb-0">Booked</h6>
                            <div class="d-flex">
                                <p class="mb-0 me-3">{{ $bookedCount }}</p>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between mb-3 pb-1">
                            <h6 class="mb-0">Available</h6>
                            <div class="d-flex">
                                <p class="mb-0 me-3">{{ $availableCount }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-xl-6 col-sm-12 order-1 order-lg-2 mb-4 mb-lg-0">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h5 class="card-title m-0 me-2">Booking Summary</h5>
                    </div>
                    <div class="card-datatable table-responsive">
                        <table class="datatables-booking table">
                            <thead class="border-top">
                                <tr>
                                    <th class="not_include"></th>
                                    <th>Room Name</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Custommer Name</th>
                                    <th>Custommer Email</th>
                                    <th>Custommer Cnic</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Last Transaction -->
            <div class="col-lg-6 mb-4 mb-lg-0">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h5 class="card-title m-0 me-2">New Customer List</h5>
                    </div>
                    <div class="card-datatable table-responsive">
                        <table class="datatables-customer table">
                            <thead class="border-bottom">
                                <tr>
                                    <th>Type</th>
                                    <th>Room Type</th>
                                    <th>Name</th>
                                    <th>phone Number</th>
                                    <th>Cnic</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        var propertyId = '{!! request()->route('property')->id !!}';
        $(document).ready(function() {
            var bookingurlurl = "{{ route('bookingsummary', ['property' => ':propertyId']) }}";
            bookingUrl = bookingurlurl.replace(':propertyId', propertyId);

            dataTable = $('.datatables-booking').DataTable({
                processing: true,
                serverSide: true,
                ajax: bookingUrl,
                columns: [
                    // columns according to JSON
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'fromDateTime'
                    },
                    {
                        data: 'toDateTime'
                    },
                    {
                        data: 'custommer_name'
                    },
                    {
                        data: 'email'
                    },
                    {
                        data: 'cnic'
                    },
                ],
                columnDefs: [{
                    // For Responsive
                    className: 'control',
                    searchable: false,
                    orderable: false,
                    responsivePriority: 2,
                    targets: 0,
                    render: function render(data, type, full, meta) {
                        return '';
                    }
                }, ],
                order: [
                    [2, 'desc']
                ],
                dom: '<"row mx-2"' + '<"col-md-2"<"me-3"l>>' +
                    '<"col-md-10"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"fB>>' +
                    '>t' + '<"row mx-2"' + '<"col-sm-12 col-md-6"i>' + '<"col-sm-12 col-md-6"p>' + '>',
                language: {
                    sLengthMenu: '_MENU_',
                    search: '',
                    searchPlaceholder: 'Search..'
                },
                // Buttons with Dropdown
                buttons: [],
                // For responsive popup
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function header(row) {
                                var data = row.data();
                                return 'Details of ' + data['name'];
                            }
                        }),
                        type: 'column',
                        renderer: function renderer(api, rowIdx, columns) {
                            var data = $.map(columns, function(col, i) {
                                return col.title !==
                                    '' // ? Do not show row in modal popup if title is blank (for check box)
                                    ?
                                    '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' +
                                    col
                                    .columnIndex + '">' + '<td>' + col.title + ':' + '</td> ' +
                                    '<td>' + col.data + '</td>' + '</tr>' : '';
                            }).join('');
                            return data ? $('<table class="table"/><tbody />').append(data) : false;
                        }
                    }
                }
            });

            var customerurl = "{{ route('property.dashboard', ['property' => ':propertyId']) }}";
            customerUrl = customerurl.replace(':propertyId', propertyId);
            dataTable = $('.datatables-customer').DataTable({
                processing: true,
                serverSide: true,
                ajax: customerUrl,
                columns: [
                    // columns according to JSON
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'type'
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'email'
                    },
                    {
                        data: 'cnic'
                    },
                ],
                columnDefs: [{
                    // For Responsive
                    className: 'control',
                    searchable: false,
                    orderable: false,
                    responsivePriority: 2,
                    targets: 0,
                    render: function render(data, type, full, meta) {
                        return '';
                    }
                }, ],
                order: [
                    [2, 'desc']
                ],
                dom: '<"row mx-2"' + '<"col-md-2"<"me-3"l>>' +
                    '<"col-md-10"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"fB>>' +
                    '>t' + '<"row mx-2"' + '<"col-sm-12 col-md-6"i>' + '<"col-sm-12 col-md-6"p>' + '>',
                language: {
                    sLengthMenu: '_MENU_',
                    search: '',
                    searchPlaceholder: 'Search..'
                },
                // Buttons with Dropdown
                buttons: [],
                // For responsive popup
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function header(row) {
                                var data = row.data();
                                return 'Details of ' + data['name'];
                            }
                        }),
                        type: 'column',
                        renderer: function renderer(api, rowIdx, columns) {
                            var data = $.map(columns, function(col, i) {
                                return col.title !==
                                    '' // ? Do not show row in modal popup if title is blank (for check box)
                                    ?
                                    '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' +
                                    col
                                    .columnIndex + '">' + '<td>' + col.title + ':' + '</td> ' +
                                    '<td>' + col.data + '</td>' + '</tr>' : '';
                            }).join('');
                            return data ? $('<table class="table"/><tbody />').append(data) : false;
                        }
                    }
                }
            });
        });
        (function() {
            let cardColor, headingColor, labelColor, borderColor, legendColor;

            if (isDarkStyle) {
                cardColor = config.colors_dark.cardColor;
                headingColor = config.colors_dark.headingColor;
                labelColor = config.colors_dark.textMuted;
                legendColor = config.colors_dark.bodyColor;
                borderColor = config.colors_dark.borderColor;
            } else {
                cardColor = config.colors.cardColor;
                headingColor = config.colors.headingColor;
                labelColor = config.colors.textMuted;
                legendColor = config.colors.bodyColor;
                borderColor = config.colors.borderColor;
            }

            // Color constant
            const chartColors = {
                column: {
                    series1: '#826af9',
                    series2: '#d2b0ff',
                    bg: '#f8d3ff'
                },
                donut: {
                    series1: '#fee802',
                    series2: '#3fd0bd',
                    series3: '#826bf8',
                    series4: '#2b9bf4'
                },
                area: {
                    series1: '#29dac7',
                    series2: '#60f2ca',
                    series3: '#a5f8cd'
                }
            };

            // Generated Leads Chart
            // --------------------------------------------------------------------
            const generatedLeadsChartEl = document.querySelector('#generatedLeadsChart'),
                generatedLeadsChartConfig = {
                    chart: {
                        height: 147,
                        width: 130,
                        parentHeightOffset: 0,
                        type: 'donut'
                    },
                    labels: ['Electronic', 'Sports', 'Decor', 'Fashion'],
                    series: [45, 58, 30, 50],
                    colors: [
                        chartColors.donut.series1,
                        chartColors.donut.series2,
                        chartColors.donut.series3,
                        chartColors.donut.series4
                    ],
                    stroke: {
                        width: 0
                    },
                    dataLabels: {
                        enabled: false,
                        formatter: function(val, opt) {
                            return parseInt(val) + '%';
                        }
                    },
                    legend: {
                        show: false
                    },
                    tooltip: {
                        theme: false
                    },
                    grid: {
                        padding: {
                            top: 15,
                            right: -20,
                            left: -20
                        }
                    },
                    states: {
                        hover: {
                            filter: {
                                type: 'none'
                            }
                        }
                    },
                    plotOptions: {
                        pie: {
                            donut: {
                                size: '70%',
                                labels: {
                                    show: true,
                                    value: {
                                        fontSize: '1.375rem',
                                        fontFamily: 'Public Sans',
                                        color: headingColor,
                                        fontWeight: 500,
                                        offsetY: -15,
                                        formatter: function(val) {
                                            return parseInt(val) + '%';
                                        }
                                    },
                                    name: {
                                        offsetY: 20,
                                        fontFamily: 'Public Sans'
                                    },
                                    total: {
                                        show: true,
                                        showAlways: true,
                                        color: config.colors.success,
                                        fontSize: '.8125rem',
                                        label: 'Total',
                                        fontFamily: 'Public Sans',
                                        formatter: function(w) {
                                            return '184';
                                        }
                                    }
                                }
                            }
                        }
                    },
                    responsive: [{
                            breakpoint: 1025,
                            options: {
                                chart: {
                                    height: 172,
                                    width: 160
                                }
                            }
                        },
                        {
                            breakpoint: 769,
                            options: {
                                chart: {
                                    height: 178
                                }
                            }
                        },
                        {
                            breakpoint: 426,
                            options: {
                                chart: {
                                    height: 147
                                }
                            }
                        }
                    ]
                };
            if (typeof generatedLeadsChartEl !== undefined && generatedLeadsChartEl !== null) {
                const generatedLeadsChart = new ApexCharts(generatedLeadsChartEl, generatedLeadsChartConfig);
                generatedLeadsChart.render();
            }

            // Donut Chart
            // --------------------------------------------------------------------
            const donutChartEl = document.querySelector('#donutChart'),
                donutChartConfig = {
                    chart: {
                        height: 390,
                        type: 'donut'
                    },
                    labels: ['Female', 'Male', 'Other'],
                    series: [{!! $data_string !!}],
                    colors: [
                        chartColors.donut.series1,
                        chartColors.donut.series4,
                        chartColors.donut.series3,
                        chartColors.donut.series2
                    ],
                    stroke: {
                        show: false,
                        curve: 'straight'
                    },
                    dataLabels: {
                        enabled: true,
                        formatter: function(val, opt) {
                            return parseInt(val, 10) + '%';
                        }
                    },
                    legend: {
                        show: true,
                        position: 'bottom',
                        markers: {
                            offsetX: -3
                        },
                        itemMargin: {
                            vertical: 3,
                            horizontal: 10
                        },
                        labels: {
                            colors: legendColor,
                            useSeriesColors: false
                        }
                    },
                    plotOptions: {
                        pie: {
                            donut: {
                                labels: {
                                    show: true,
                                    name: {
                                        fontSize: '2rem',
                                        fontFamily: 'Public Sans'
                                    },
                                    value: {
                                        fontSize: '1.2rem',
                                        color: legendColor,
                                        fontFamily: 'Public Sans',
                                        formatter: function(val) {
                                            return parseInt(val, 10) + '%';
                                        }
                                    },
                                    total: {
                                        show: false,
                                        fontSize: '1.5rem',
                                        color: headingColor,
                                        label: 'Operational',
                                        formatter: function(w) {
                                            return '42%';
                                        }
                                    }
                                }
                            }
                        }
                    },
                    responsive: [{
                            breakpoint: 992,
                            options: {
                                chart: {
                                    height: 380
                                },
                                legend: {
                                    position: 'bottom',
                                    labels: {
                                        colors: legendColor,
                                        useSeriesColors: false
                                    }
                                }
                            }
                        },
                        {
                            breakpoint: 576,
                            options: {
                                chart: {
                                    height: 320
                                },
                                plotOptions: {
                                    pie: {
                                        donut: {
                                            labels: {
                                                show: true,
                                                name: {
                                                    fontSize: '1.5rem'
                                                },
                                                value: {
                                                    fontSize: '1rem'
                                                },
                                                total: {
                                                    fontSize: '1.5rem'
                                                }
                                            }
                                        }
                                    }
                                },
                                legend: {
                                    position: 'bottom',
                                    labels: {
                                        colors: legendColor,
                                        useSeriesColors: false
                                    }
                                }
                            }
                        },
                        {
                            breakpoint: 420,
                            options: {
                                chart: {
                                    height: 280
                                },
                                legend: {
                                    show: false
                                }
                            }
                        },
                        {
                            breakpoint: 360,
                            options: {
                                chart: {
                                    height: 250
                                },
                                legend: {
                                    show: false
                                }
                            }
                        }
                    ]
                };
            if (typeof donutChartEl !== undefined && donutChartEl !== null) {
                const donutChart = new ApexCharts(donutChartEl, donutChartConfig);
                donutChart.render();
            }
        })();
    </script>
@endsection
