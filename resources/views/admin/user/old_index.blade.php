@extends('admin.layouts.master')
@section('title', 'Users')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-2 mb-1"><span class="text-muted fw-light">Users /</span> List</h4>
        <div class="card">
            <div class="card-datatable table-responsive">
                <table class="datatables-users table">
                    <thead class="border-top">
                        <tr>
                            <th class="not_include"></th>
                            <th>Sr.#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <!-- Offcanvas to add new user -->
            <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasAddUser"
                aria-labelledby="offcanvasAddUserLabel">
                <div class="offcanvas-header">
                    <h5 id="offcanvasAddUserLabel" class="offcanvas-title">Add User</h5>
                    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas"
                        aria-label="Close"></button>
                </div>
                <div class="offcanvas-body mx-0 flex-grow-0 pt-0 h-100">
                    <form class="form pt-0" id="add_form">
                        @csrf
                        <div class="mb-3">
                            <label class="form-label" for="add-user-fullname"> Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Enter Name" required />
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="email">Email</label>
                            <input type="text" class="form-control" name="email" placeholder="john.doe@example.com"
                                autocomplete="username" required />
                        </div>
                        <div class="mb-3">
                            <div class="form-password-toggle">
                                <label class="form-label" for="password">Password</label>
                                <div class="input-group input-group-merge">
                                    <input type="password" id="multicol-password" name="password" class="form-control"
                                        placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                        autocomplete="new-password" required />
                                    <span class="input-group-text cursor-pointer" id="multicol-password2"><i
                                            class="ti ti-eye-off"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form-password-toggle">
                                <label class="form-label" for="password_confirmation">Confirm Password</label>
                                <div class="input-group input-group-merge">
                                    <input type="password" id="password_confirmation" name="password_confirmation"
                                        class="form-control"
                                        placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                        autocomplete="new-password" required />
                                    <span class="input-group-text cursor-pointer" id="passwordConfirmation"><i
                                            class="ti ti-eye-off"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="user-role">User Role</label>
                            <select id="user-role" name="role" class="form-select select2" data-placeholder="Select Role"
                                required>
                                @foreach ($roles as $role)
                                    <option value=""></option>
                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                        <button type="reset" class="btn btn-label-secondary" data-bs-dismiss="offcanvas">Cancel</button>
                    </form>
                </div>
            </div>
            <!-- Offcanvas to Edit user -->
            <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasEditUser"
                aria-labelledby="offcanvasEditUserLabel">
                <div class="offcanvas-header">
                    <h5 id="offcanvasEditUserLabel" class="offcanvas-title">Edit User</h5>
                    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas"
                        aria-label="Close"></button>
                </div>
                <div class="offcanvas-body mx-0 flex-grow-0 pt-0 h-100">
                    <form class="form pt-0" id="update_form">
                        @csrf
                        @method('PUT')
                        <div class="mb-3">
                            <label class="form-label" for="add-user-fullname"> Name</label>
                            <input type="text" class="form-control" id="edit_name" name="name"
                                placeholder="Enter Name" />
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="add-user-email">Email</label>
                            <input type="text" class="form-control" id="edit_email" name="email"
                                placeholder="john.doe@example.com" autocomplete="username" />
                        </div>
                        <div class="mb-3">
                            <div class="form-password-toggle">
                                <label class="form-label" for="edit_multicol-password">Password</label>
                                <div class="input-group input-group-merge">
                                    <input type="password" id="edit_multicol-password" name="password"
                                        class="form-control"
                                        placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                        aria-describedby="edit_pass" autocomplete="current-password" />
                                    <span class="input-group-text cursor-pointer" id="edit_pass"><i
                                            class="ti ti-eye-off"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form-password-toggle">
                                <label class="form-label" for="edit_password_confirmation">Confirm Password</label>
                                <div class="input-group input-group-merge">
                                    <input type="password" id="edit_password_confirmation" name="password_confirmation"
                                        class="form-control"
                                        placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                        aria-describedby="passwordConfirmation" autocomplete="current-password" />
                                    <span class="input-group-text cursor-pointer" id="passwordConfirmation"><i
                                            class="ti ti-eye-off"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="edit_role">User Role</label>
                            <select id="edit_role" name="role" class="select2 form-select"
                                data-placeholder="Select Role" required>
                                @foreach ($roles as $role)
                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary me-sm-3 me-1">Update</button>
                        <button type="reset" class="btn btn-label-secondary"
                            data-bs-dismiss="offcanvas">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="sk-wave sk-primary" id="loader"
            style="display: none; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
            <div class="sk-wave-rect"></div>
        </div>
    </div>

@section('scripts')
    <script>
        $(document).ready(function() {
            dataTable = $('.datatables-users').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('users.index') }}",
                columns: [
                    // columns according to JSON
                    {
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'name'
                    }, {
                        data: 'email'
                    }, {
                        name: 'roles.name',
                        render: function(data, type, full, meta) {
                            let role = full['roles'];
                            let roles = '';
                            if (role.length < 1) {
                                return '-';
                            }

                            $.each(role, function(index, value) {
                                roles +=
                                    '<span class="badge rounded-pill bg-label-success ms-1 mb-1">' +
                                    value.name + '</span>';
                            });

                            return roles
                        }
                    }, {
                        data: 'action'
                    }
                ],
                columnDefs: [{
                    // For Responsive
                    className: 'control',
                    searchable: false,
                    orderable: false,
                    responsivePriority: 2,
                    targets: 0,
                    render: function render(data, type, full, meta) {
                        return '';
                    }
                }, {
                    // Actions
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function render(data, type, full, meta) {
                        let role = full['roles'];
                        var actions = '<div class="d-inline-block text-nowrap">' +
                            '<a href="javascript:;" class="text-body" onclick=userEdit(' + full
                            .id + ')>' + '<i class=\"ti ti-edit\"></i>' +
                            '</a>';

                        // Check if the user is not a superadmin before rendering the delete button
                        if (role.length > 0) {
                            if (role[0].name !== 'Super Admin') {
                                actions +=
                                    '<a href="javascript:;" class="text-body" onclick=deleteUser(' +
                                    full.id + ')>' + '<i class=\"ti ti-trash\"></i>' +
                                    '</a>';
                            }
                        }

                        actions += '</div>';

                        return actions;
                    }
                }],
                order: [
                    [2, 'desc']
                ],
                dom: '<"row mx-2"' + '<"col-md-2"<"me-3"l>>' +
                    '<"col-md-10"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"fB>>' +
                    '>t' + '<"row mx-2"' + '<"col-sm-12 col-md-6"i>' + '<"col-sm-12 col-md-6"p>' + '>',
                language: {
                    sLengthMenu: '_MENU_',
                    search: '',
                    searchPlaceholder: 'Search..'
                },
                // Buttons with Dropdown
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-label-primary dropdown-toggle mx-3',
                    text: '<i class="ti ti-logout rotate-n90 me-2"></i>Export',
                    buttons: [{
                        extend: 'print',
                        title: 'Users',
                        text: '<i class="ti ti-printer me-2" ></i>Print',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be print
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList !== undefined && item
                                            .classList.contains('user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        },
                        customize: function customize(win) {
                            //customize print view for dark
                            $(win.document.body).css('color', config.colors
                                    .headingColor)
                                .css('border-color', config.colors.borderColor).css(
                                    'background-color', config.colors.body);
                            $(win.document.body).find('table').addClass('compact').css(
                                    'color', 'inherit').css('border-color', 'inherit')
                                .css(
                                    'background-color', 'inherit');
                        }
                    }, {
                        extend: 'csv',
                        title: 'Users',
                        text: '<i class="ti ti-file-text me-2" ></i>Csv',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be print
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                            'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'excel',
                        title: 'Users',
                        text: '<i class="ti ti-file-spreadsheet me-2"></i>Excel',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be display
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                            'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'pdf',
                        title: 'Users',
                        text: '<i class="ti ti-file-text me-2"></i>Pdf',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be display
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                            'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }, {
                        extend: 'copy',
                        title: 'Users',
                        text: '<i class="ti ti-copy me-1" ></i>Copy',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [2, 3],
                            // prevent avatar to be copy
                            format: {
                                body: function body(inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function(index, item) {
                                        if (item.classList.contains(
                                            'user-name')) {
                                            result = result + item.lastChild
                                                .textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }]
                }, {
                    text: '<i class="ti ti-plus me-0 me-sm-1"></i><span class="d-none d-sm-inline-block">Add New</span>',
                    className: 'add-new btn btn-primary',
                    attr: {
                        'data-bs-toggle': 'offcanvas',
                        'data-bs-target': '#offcanvasAddUser'
                    }
                }],
                // For responsive popup
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function header(row) {
                                var data = row.data();
                                return 'Details of ' + data['name'];
                            }
                        }),
                        type: 'column',
                        renderer: function renderer(api, rowIdx, columns) {
                            var data = $.map(columns, function(col, i) {
                                return col.title !==
                                    '' // ? Do not show row in modal popup if title is blank (for check box)
                                    ?
                                    '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' +
                                    col
                                    .columnIndex + '">' + '<td>' + col.title + ':' + '</td> ' +
                                    '<td>' + col.data + '</td>' + '</tr>' : '';
                            }).join('');
                            return data ? $('<table class="table"/><tbody />').append(data) : false;
                        }
                    }
                }
            });

            $("#add_form").submit(function(e) {
                $('#loader').show();
                e.preventDefault();
                $.ajax({
                    url: "{{ route('users.store') }}",
                    type: "POST",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function success(response) {
                        // sweetalert
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Swal.fire({
                                    title: value,
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            });
                        } else if (response.error_message) {
                            swal.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form')[0].reset();
                            dataTable.ajax.reload();
                            $('#loader').hide();
                            $('#offcanvasAddUser').offcanvas('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'Users has been Added Successfully!',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        }
                    },
                });
            });

            $("#update_form").submit(function(e) {
                $('#loader').show();
                e.preventDefault();
                $.ajax({
                    url: "{{ url('users') }}" + "/" + rowid,
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function success(response) {
                        // sweetalert
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Swal.fire({
                                    title: value,
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            });
                        } else if (response.error_message) {
                            swal.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            dataTable.ajax.reload();
                            $('#loader').hide();
                            $('#offcanvasEditUser').offcanvas('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'Users has been Updated Successfully!',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        }
                    },
                });
            });
        });

        function userEdit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('users') }}" + "/" + rowid + "/edit",
                type: "GET",
                success: function(response) {
                    var role = response.role ? response.role.role_id : '';
                    $('#edit_name').val(response.user.name);
                    $('#edit_email').val(response.user.email);
                    $('#edit_role').val(response.role);
                    $('#offcanvasEditUser').offcanvas('show');
                }
            });
        }

        function deleteUser(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "users/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        dataTable.ajax.reload();
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'User has been Deleted Successfully!',
                                            customClass: {
                                                confirmButton: 'btn btn-success'
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection

@endsection
