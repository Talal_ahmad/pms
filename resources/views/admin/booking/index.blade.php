@extends('admin.layouts.master')
@section('title', 'Room Reservation')
@section('content')

    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <div class="rounded m-3 p-2">
                <div class="row">
                    <div class="d-flex justify-content-between">
                        <div class="col-6">
                            <h4 class="py-2 mb-1"><span class="text-muted fw-light">Room Reservation /</span> List</h4>
                        </div>
                        <div class="col-6">
                            <div class="row">
                                <div class="col-8">
                                    <div class="border rounded-pill ss">
                                        <div class="input-group">
                                            <a href="#">
                                                <div class="input-group-append">
                                                    <span class="btn rounded-pill border" type="submit">
                                                        <i class="fa-solid fa-magnifying-glass fa-flip-horizontal"></i>
                                                    </span>
                                                </div>
                                            </a>
                                            <input type="text" class="form-control rounded-pill border-0 "
                                                id="anythingSearch" placeholder="Search">
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="col-4 d-flex justify-content-end">
                                    <button type="button" class="btn btn-success text-white" style="font-family: Inter;">
                                        + Add Reservation
                                    </button>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mt-1">
            <div class="m-3 rounded">
                <div class="container">
                    <div class="row">
                        <ul class="nav nav-pills mt-1" id="pills-tab" role="tablist">
                            <div>
                                <h3 class="mb-0 " style="font-family: Montserrat; font-weight: 600; font-size: 24px;">
                                    Floor Plan:
                                </h3>
                            </div>
                            @foreach ($floors as $floor)
                                <li class="nav-item ms-4 mx-2 mb-0" role="presentation">
                                    <a class="nav-link mb-0 btn rounded-pill fw-bold{{ $loop->first ? ' active border-dark' : '' }}"
                                        id="floor-{{ $floor->id }}-tab" data-bs-toggle="pill"
                                        data-bs-target="#floor-{{ $floor->id }}" type="button" role="tab"
                                        aria-controls="floor-{{ $floor->id }}" aria-selected="{{ $loop->first }}">
                                        {{ $floor->name }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div id="myDIV" class="row">
                        <div class="tab-content" id="pills-tabContent">
                            @foreach ($floors as $floor)
                                <div class="tab-pane fade show{{ $loop->first ? ' active' : '' }}"
                                    id="floor-{{ $floor->id }}" role="tabpanel">
                                    <div class="row mt-1">
                                        @foreach ($floor->rooms as $room)
                                            @if ($room->status == 'Booked')
                                                {{-- Reserved new --}}
                                                <div class="col-12 col-sm-6 col-md-3 col-lg-4 rounded "
                                                    style="border-radius: 10px;">
                                                    <div class="card p-0 rounded-4 mt-4" style="border-radius: 10px;">
                                                        <div class="row">
                                                            <div class="col-7"></div>
                                                            <div class="col-4 d-flex justify-content-center rounded-bottom-pill  text-white mb-0 pb-0"
                                                                style="background-color: #E72F60; border-radius: 0 0 13px 13px;">
                                                                <p class="mb-0 pb-1">Reserved</p>
                                                            </div>
                                                        </div>
                                                        <div class="row m-1 mb-0">
                                                            <div class="col-3">
                                                                <img src="{{ asset('images/roomsRed.png') }}"
                                                                    alt="">
                                                            </div>

                                                            <div class="col-5 mt-2">
                                                                <h5 class="m-0 p-0">{{ $room->name }}</h5>
                                                            </div>
                                                            <div class="col-3 mt-1">
                                                                {{-- <button type="button" class="btn btn-info">Rent</button> --}}
                                                                <a href="{{ url('properties/' . request()->route('property')->id . '/Createbooking/' . $room->id) }}"class="btn btn-success px-3 rounded border-0 mb-4 mt-2 "
                                                                    style="background-color: #6366F1; font-family: Poppins; font-weight: 400; font-size: 15px; ">Advance</a>
                                                            </div>
                                                        </div>
                                                        @foreach ($room->bookings as $booking)
                                                            <div class="row">
                                                                <div class="col-12 d-flex justify-content-center mb-2 p-3">
                                                                    @if (!$booking->arrivalDateTime)
                                                                        <button type="button" class="btn btn-info"
                                                                            data-bs-toggle="modal"
                                                                            data-bs-target="#arrivalDateTime_modal_{{ $booking->id }}">
                                                                            check_in
                                                                        </button>
                                                                        {{-- arrivalDateTime modal --}}
                                                                        <div class="modal fade"
                                                                            id="arrivalDateTime_modal_{{ $booking->id }}"
                                                                            tabindex="-1" aria-hidden="true">
                                                                            <div
                                                                                class="modal-dialog modal-dialog-centered1  modal-simple modal-add-new-cc">
                                                                                <div class="modal-content p-3 p-md-5">
                                                                                    <div class="modal-body">
                                                                                        <button type="button"
                                                                                            class="btn-close"
                                                                                            data-bs-dismiss="modal"
                                                                                            aria-label="Close"></button>
                                                                                        <div class="text-center mb-4">
                                                                                            <h3 class="mb-2">Add Arrival
                                                                                                Date
                                                                                            </h3>
                                                                                        </div>
                                                                                        {{-- Modify the form ID and add a class to distinguish --}}
                                                                                        <form
                                                                                            class="form row g-3 arrivalDateTimeForm"
                                                                                            id="arrivalDateTime_form_{{ $booking->id }}"
                                                                                            data-booking-id="{{ $booking->id }}">
                                                                                            @csrf
                                                                                            <div class="col-12 mb-2">
                                                                                                <input type="hidden"
                                                                                                    name="booking_id"
                                                                                                    value="{{ $booking->id }}">
                                                                                                <label class="form-label"
                                                                                                    for="arrivalDateTime">Arrival
                                                                                                    Date & Time</label>
                                                                                                <input type="datetime-local"
                                                                                                    class="form-control"
                                                                                                    id="arrivalDateTime"
                                                                                                    name="arrivalDateTime"
                                                                                                    placeholder="Enter Date Time"
                                                                                                    required />
                                                                                            </div>
                                                                                            <button type="submit"
                                                                                                class="btn btn-primary me-sm-3 me-1">Submit</button>
                                                                                            <button type="reset"
                                                                                                class="btn btn-label-secondary"
                                                                                                data-bs-dismiss="offcanvas">Cancel</button>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @elseif (!$booking->departureDateTime)
                                                                        <div class="d-flex justify-content-between w-100"  style="max-width: 250px;">
                                                                            <button type="button" class="btn btn-success"
                                                                                data-bs-toggle="modal"
                                                                                data-bs-target="#rent_modal_{{ $booking->id }}">Vehicle
                                                                                Rent
                                                                            </button>
        
                                                                            <div class="modal fade"
                                                                                id="rent_modal_{{ $booking->id }}"
                                                                                tabindex="-1" aria-hidden="true">
                                                                                <div
                                                                                    class="modal-dialog modal-dialog-centered1 modal-lg modal-simple modal-add-new-cc">
                                                                                    <div class="modal-content p-3 p-md-5">
                                                                                        <div class="modal-body">
                                                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                                            <div class="text-center mb-4">
                                                                                                <h3 class="mb-2">Rent Vehicle</h3>
                                                                                            </div>
                                                                                            <form class="form row g-3 rentbookingForm"  id="rent_form_{{ $booking->id }}"
                                                                                                data-booking-id="{{ $booking->id }}">
                                                                                                @csrf
                                                                                                <div class="col-6 mt-2">
                                                                                                    <label class="form-label"
                                                                                                        for="vehicle_id">Vehicle</label>
                                                                                                    <select id="vehicle_id" name="vehicle_id" class="form-select select2" data-placeholder="Select vehicle">
                                                                                                        <option value=""></option>
                                                                                                        @foreach ($vehicles as $vehicle)
                                                                                                            <option value="{{ $vehicle->id }}">
                                                                                                                {{ $vehicle->name }}
                                                                                                            </option>
                                                                                                        @endforeach
                                                                                                    </select>
                                                                                                </div>
                                                                                                <div class="col-6 mt-2">
                                                                                                    <label class="form-label"
                                                                                                        for="fromRentDateTime">From
                                                                                                        Date & Time</label>
                                                                                                    <input
                                                                                                        type="date"
                                                                                                        class="form-control"
                                                                                                        id="fromRentDateTime"
                                                                                                        name="fromRentDateTime"
                                                                                                        placeholder="Enter Rent Date & Time"
                                                                                                        required />
                                                                                                </div>
                                                                                                <div class="col-6 mt-2">
                                                                                                    <label class="form-label"
                                                                                                        for="toRentDateTime">To
                                                                                                        Date & Time</label>
                                                                                                    <input
                                                                                                        type="date"
                                                                                                        class="form-control"
                                                                                                        id="toRentDateTime"
                                                                                                        name="toRentDateTime"
                                                                                                        placeholder="Enter Date & Time"
                                                                                                        required />
                                                                                                </div>
                                                                                                <div class="col-6 mt-4 d-flex align-items-center">
                                                                                                    <input type="radio" id="driver" name="package_type" value="driver">
                                                                                                    <label for="driver" class="ms-2 me-3">Driver</label>
                                                                                                    <input type="radio" id="with_out_driver" name="package_type" value="with_out_driver">
                                                                                                    <label for="with_out_driver" class="ms-2">With Out Driver</label>
                                                                                                </div>
                                                                                                <input type="hidden" name="booking_id" value="{{ $booking->id }}">
                                                                                                <div class="col-12 text-center">
                                                                                                    <button type="submit" class="btn btn-primary me-sm-3 me-1">Update</button>
                                                                                                    <button type="reset" class="btn btn-label-secondary btn-reset"
                                                                                                        data-bs-dismiss="modal" aria-label="Close">
                                                                                                        Cancel
                                                                                                    </button>
                                                                                                </div>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <button type="button" class="btn btn-info "
                                                                                data-bs-toggle="modal"
                                                                                data-bs-target="#departureDateTime_modal_{{ $booking->id }}">
                                                                                check_out
                                                                            </button>
                                                                            {{-- arrivalDateTime modal --}}
                                                                            <div class="modal fade"
                                                                                id="departureDateTime_modal_{{ $booking->id }}"
                                                                                tabindex="-1" aria-hidden="true">
                                                                                <div
                                                                                    class="modal-dialog modal-dialog-centered1  modal-simple modal-add-new-cc">
                                                                                    <div class="modal-content p-3 p-md-5">
                                                                                        <div class="modal-body">
                                                                                            <button type="button"
                                                                                                class="btn-close"
                                                                                                data-bs-dismiss="modal"
                                                                                                aria-label="Close"></button>
                                                                                            <div class="text-center mb-4">
                                                                                                <h3 class="mb-2">Add
                                                                                                    Departure DateTime
                                                                                                </h3>
                                                                                            </div>
                                                                                            <form
                                                                                                class="form row g-3 departureDateTimeForm"
                                                                                                id="departureDateTime_form_{{ $booking->id }}"
                                                                                                data-booking-id="{{ $booking->id }}">
                                                                                                @csrf
                                                                                                <div class="col-12 mb-2">
                                                                                                    <input type="hidden"
                                                                                                        name="booking_id"
                                                                                                        value="{{ $booking->id }}">
                                                                                                    <input type="hidden"
                                                                                                        name="room_id"
                                                                                                        value="{{ $room->id }}">
                                                                                                    <label class="form-label"
                                                                                                        for="departureDateTime">Departure
                                                                                                        Date & Time</label>
                                                                                                    <input
                                                                                                        type="datetime-local"
                                                                                                        class="form-control"
                                                                                                        id="departureDateTime"
                                                                                                        name="departureDateTime"
                                                                                                        placeholder="Enter Departure Date Time"
                                                                                                        required />
                                                                                                </div>
                                                                                                <button type="submit"
                                                                                                    class="btn btn-primary me-sm-3 me-1">Submit</button>
                                                                                                <button type="reset"
                                                                                                    class="btn btn-label-secondary"
                                                                                                    data-bs-dismiss="offcanvas">Cancel</button>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class=" d-flex justify-content-center">
                                                                    <p class="" style="font-size: 14px;">
                                                                        <b>From Date:</b>
                                                                        {{ $booking->fromDateTime }} &nbsp; &nbsp;
                                                                        &nbsp;<br>
                                                                        <b>To Date:</b>
                                                                        {{ $booking->toDateTime }}&nbsp; &nbsp;
                                                                        &nbsp;<br>
                                                                        <b>Arrival Date:</b>
                                                                        {{ $booking->arrivalDateTime ? $booking->arrivalDateTime : '' }}&nbsp;
                                                                        &nbsp;
                                                                        &nbsp;
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="card-body pt-0">
                                                                <div
                                                                    class="card-text d-flex justify-content-between  mb-2">
                                                                    <div>
                                                                        <span
                                                                            class="ps-1 font-family-poppins d-block text-truncate"
                                                                            style="font-family: Poppins; font-weight: 700; font-size: 13px;">Name:
                                                                        </span>
                                                                    </div>
                                                                    <div
                                                                        style="font-family: Poppins; font-weight: 700; font-size: 13px;">
                                                                        {{ $booking->customer->name }}
                                                                    </div>
                                                                </div>

                                                                <div
                                                                    class="card-text d-flex justify-content-between  mb-2">
                                                                    <div><span
                                                                            class="ps-1  font-family-poppins d-block text-truncate"
                                                                            style="font-family: Poppins; font-weight: 700; font-size: 13px;">Contact:
                                                                        </span></div>
                                                                    <div
                                                                        style="font-family: Poppins; font-weight: 700; font-size: 13px;">
                                                                        {{ $booking->customer->phone_number }}
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="card-text d-flex justify-content-between  mb-2">
                                                                    <div><span
                                                                            class="ps-1  font-family-poppins d-block text-truncate"
                                                                            style="font-family: Poppins; font-weight: 700; font-size: 13px;">Email:
                                                                        </span></div>
                                                                    <div
                                                                        style="font-family: Poppins; font-weight: 700; font-size: 13px;">
                                                                        {{ $booking->customer->email }}
                                                                    </div>
                                                                </div>
                                                                {{-- <div class="card-text d-flex justify-content-between  mb-2">
                                                                    <div><span
                                                                            class="ps-1 font-family-poppins d-block text-truncate"
                                                                            style="font-family: Poppins; font-weight: 700; font-size: 13px;">Guest:
                                                                        </span></div>
                                                                    <div
                                                                        style="font-family: Poppins; font-weight: 700; font-size: 12px;">
                                                                        {{ $booking->customer->name }}
                                                                    </div>
                                                                </div> --}}
                                                                <hr>
                                                                <div
                                                                    class="text-muted card-text d-flex justify-content-between  ">
                                                                    <div class="ps-1 font-family-poppins d-block text-truncate"
                                                                        style="font-family: Poppins; font-weight: 400; font-size: 13px; white-space: nowrap">
                                                                        {{ $booking->description }}
                                                                    </div>
                                                                </div>
                                                                @if (!$booking->departureDateTime)
                                                                    <div class="row d-flex align-items-center mt-2">
                                                                        <div class=" col-5 d-inline-block">
                                                                            <a href="{{ url('properties/' . request()->route('property')->id . '/Editbooking/' . $booking->id) }}"
                                                                                class="btn btn-primary">
                                                                                Edit
                                                                            </a>
                                                                        </div>
                                                                        <div class="col-7 d-inline-block">
                                                                            <button class="btn btn-danger"
                                                                                onclick="cancelBooking({{ $booking->id }})">Booking
                                                                                Cancel</button>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @elseif ($room->status == 'occupied')
                                                <div class="col-12 col-sm-6 col-md-3 col-lg-4 rounded "
                                                    style="border-radius: 10px;">
                                                    <div class="card p-0 rounded-4 mt-4 " style="border-radius: 10px; height: 450px;">
                                                        <div>
                                                            <div class="row">
                                                                <div class="col-7"></div>
                                                                <div class="col-4 d-flex justify-content-center rounded-bottom text-white mb-0 pb-0"
                                                                    style="background-color: #E72F60;">
                                                                    <p class="mb-0 pb-1">Dirty</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mt-3 mb-0 pb-0 d-flex justify-content-center text-white"
                                                            style=" border-top-right-radius: 10px;border-top-left-radius:10px ">
                                                            <div class="d-flex justify-content-center text-dark  "
                                                                style="font-family: Poppins; font-weight: 500; font-size: 18px;">
                                                                {{ $room->name }}
                                                            </div>
                                                        </div>
                                                        <div class="card-body mt-5 p-0">
                                                            <div class="d-flex justify-content-center"
                                                                style="height: 240px;">
                                                                @if ($room->cover_image)
                                                                    <img src="{{ asset('storage/images/rooms/' . $room->cover_image) }}"
                                                                        alt="Room Image" style="border-radius: 15px; width:90%">
                                                                @else
                                                                    <img src="{{ asset('images/roomBlue.png') }}"
                                                                        alt="Default Image">
                                                                @endif
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="col-12 col-sm-6 col-md-3 col-lg-4 rounded "
                                                    style="border-radius: 10px;">
                                                    <div class="card p-0 rounded-4 mt-4 " style="border-radius: 10px;  height: 450px;">
                                                        <div>
                                                            <div class="row">
                                                                <div class="col-7"></div>
                                                                <div class="col-4 d-flex justify-content-center rounded-bottom text-white mb-0 pb-0"
                                                                    style="background-color: #56C456;">
                                                                    <p class="mb-0 pb-1">Available</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mt-3 mb-0 pb-0 d-flex justify-content-center text-white"
                                                            style=" border-top-right-radius: 10px;border-top-left-radius:10px ">
                                                            <div class="d-flex justify-content-center text-dark  "
                                                                style="font-family: Poppins; font-weight: 500; font-size: 18px;">
                                                                {{ $room->name }}
                                                            </div>
                                                        </div>
                                                        <div class="card-body mt-5 p-0">
                                                            <div class="d-flex justify-content-center"
                                                                style="height: 250px;">
                                                                @if ($room->cover_image)
                                                                    <img src="{{ asset('storage/images/rooms/' . $room->cover_image) }}"
                                                                        alt="Room Image" style="border-radius: 15px; width:90%">
                                                                @else
                                                                    <img src="{{ asset('images/roomBlue.png') }}"
                                                                        alt="Default Image">
                                                                @endif
                                                            </div>
                                                            <div class="d-flex justify-content-center ">
                                                                <div class="mt-3">
                                                                    <a href="{{ url('properties/' . request()->route('property')->id . '/Createbooking/' . $room->id) }}"class="btn btn-success px-5 rounded border-0 mb-4 mt-2 "
                                                                        style="background-color: #6366F1; font-family: Poppins; font-weight: 400; font-size: 15px; ">Reserve
                                                                        Room</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        var propertyId = '{!! request()->route('property')->id !!}';

        $(document).ready(function() {
            $(".arrivalDateTimeForm").submit(function(e) {
                e.preventDefault(); // Prevent form submission
                $('#loader').show(); // Show the loader

                var bookId = $(this).data('booking-id'); // Get the room ID from data attribute
                var formData = new FormData(this);
                formData.append('property_id', propertyId);


                $.ajax({
                    url: "{{ url('properties') }}" + "/" + propertyId +
                        "/arrivalbooking/" + bookId,
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        $('#loader').hide();

                        if (response.errors) {
                            $.each(response.errors, function(field, errors) {
                                $.each(errors, function(index, error) {
                                    Swal.fire({
                                        title: error,
                                        icon: 'error',
                                        customClass: {
                                            confirmButton: 'btn btn-success'
                                        }
                                    });
                                });
                            });
                        } else if (response.error_message) {
                            Swal.fire({
                                icon: 'error',
                                title: 'An error occurred! Please contact the administrator.',
                                customClass: {
                                    confirmButton: 'btn btn-danger'
                                }
                            });
                        } else {
                            $('#arrivalDateTime_form_' + bookId)[0].reset();
                            window.location.reload();
                            $('#add_modal').modal('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'Booking has been Update successfully!',
                                customClass: {
                                    confirmButton: 'btn btn-success'
                                }
                            });
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $('#loader').hide();

                        if (jqXHR.status === 422) {
                            const errors = jqXHR.responseJSON.errors;
                            $.each(errors, function(field, errorList) {
                                $.each(errorList, function(index, error) {
                                    Swal.fire({
                                        title: error,
                                        icon: 'error',
                                        customClass: {
                                            confirmButton: 'btn btn-success'
                                        }
                                    });
                                });
                            });
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'An unexpected error occurred. Please try again later.',
                                customClass: {
                                    confirmButton: 'btn btn-danger'
                                }
                            });
                        }
                    }
                });
            });
        });
        $(".departureDateTimeForm").submit(function(e) {
            e.preventDefault(); // Prevent form submission
            $('#loader').show(); // Show the loader

            var bookId = $(this).data('booking-id'); // Get the room ID from data attribute
            var formData = new FormData(this);
            formData.append('property_id', propertyId);

            $.ajax({
                url: "{{ url('properties') }}" + "/" + propertyId +
                    "/departurebooking/" + bookId,
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    $('#loader').hide();

                    if (response.errors) {
                        $.each(response.errors, function(field, errors) {
                            $.each(errors, function(index, error) {
                                Swal.fire({
                                    title: error,
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            });
                        });
                    } else if (response.error_message) {
                        Swal.fire({
                            icon: 'error',
                            title: 'An error occurred! Please contact the administrator.',
                            customClass: {
                                confirmButton: 'btn btn-danger'
                            }
                        });
                    } else {
                        $('#departureDateTime_form_' + bookId)[0].reset();
                        window.location.reload();
                        $('#add_modal').modal('hide');
                        Swal.fire({
                            icon: 'success',
                            title: 'Booking has been Update successfully!',
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#loader').hide();

                    if (jqXHR.status === 422) {
                        const errors = jqXHR.responseJSON.errors;
                        $.each(errors, function(field, errorList) {
                            $.each(errorList, function(index, error) {
                                Swal.fire({
                                    title: error,
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            });
                        });
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'An unexpected error occurred. Please try again later.',
                            customClass: {
                                confirmButton: 'btn btn-danger'
                            }
                        });
                    }
                }
            });
        });

        $(".rentbookingForm").submit(function(e) {
            e.preventDefault(); // Prevent form submission
            $('#loader').show(); // Show the loader

            var bookId = $(this).data('booking-id'); // Get the room ID from data attribute
            console.log(bookId);
            var formData = new FormData(this);
            formData.append('property_id', propertyId);

            $.ajax({
                url: "{{ url('properties') }}" + "/" + propertyId +
                    "/rentbooking/" + bookId,
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    $('#loader').hide();

                    if (response.errors) {
                        $.each(response.errors, function(field, errors) {
                            $.each(errors, function(index, error) {
                                Swal.fire({
                                    title: error,
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            });
                        });
                    } else if (response.error_message) {
                        Swal.fire({
                            icon: 'error',
                            title: 'An error occurred! Please contact the administrator.',
                            customClass: {
                                confirmButton: 'btn btn-danger'
                            }
                        });
                    } else {
                        $('#departureDateTime_form_' + bookId)[0].reset();
                        window.location.reload();
                        $('#add_modal').modal('hide');
                        Swal.fire({
                            icon: 'success',
                            title: 'Rent Booking has been Update successfully!',
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#loader').hide();

                    if (jqXHR.status === 422) {
                        const errors = jqXHR.responseJSON.errors;
                        $.each(errors, function(field, errorList) {
                            $.each(errorList, function(index, error) {
                                Swal.fire({
                                    title: error,
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            });
                        });
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'An unexpected error occurred. Please try again later.',
                            customClass: {
                                confirmButton: 'btn btn-danger'
                            }
                        });
                    }
                }
            });
        });
        var propertyId = '{!! request()->route('property')->id !!}';

        function cancelBooking(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to Cancel this booking?',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "{{ url('properties') }}/" + propertyId + "/cancelbooking/" +
                                    id, // Fix URL concatenation
                                type: "POST",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has occurred! Please contact the administrator.'
                                        });
                                    } else if (response.code == "500") {
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Error!',
                                            text: response.error_message,
                                            customClass: {
                                                confirmButton: 'btn btn-danger'
                                            }
                                        });
                                    } else if (response.code == "200") {
                                        // Show success message using Swal.fire
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'Booking Cancelled Successfully!',
                                            customClass: {
                                                confirmButton: 'btn btn-success'
                                            }
                                        }).then(function() {
                                            // Reload the page after showing the success message
                                            location.reload();
                                        });
                                    } else {
                                        // Handle other response codes if needed
                                    }
                                },
                                error: function(xhr, status, error) {
                                    console.error(xhr.responseText);
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Error!',
                                        text: 'An error occurred while processing your request. Please try again later.',
                                        customClass: {
                                            confirmButton: 'btn btn-danger'
                                        }
                                    });
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
