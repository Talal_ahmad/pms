import axios from '../axios';
export default class MenuItemService {
    constructor(vm) {
        this.vm = vm;
    }
    getItems() {
        let self = this.vm;
        let loader = self.$loading.show();
        return axios
            .get(`properties/${self.$store.state.property.id}/menu-items`)
            .then(function (response) {
                self.items = response.data.data;
                loader.hide();
            })
            .catch((error) => error);
    }
    storeItem(formData) {
        let self = this.vm;
        let loader = self.$loading.show({
            container: self.$refs.addMenuItemForm
        });
        axios
        .post(`properties/${self.$store.state.property.id}/menu-items`, formData)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: 'Item Created Successfully!', life: 1000 });
                loader.hide();
                self.$router.push({name : 'menu-items'});
            })
            .catch(function (error) {
                loader.hide();
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            self.$toast.add({ severity: 'error', summary: 'Error', detail: errorDetail, life: 3000 });
                        }
                    }
                } else {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    getItem(id) {
        let self = this.vm;
        return this.vm.$axios
            .get(`properties/${self.$store.state.property.id}/menu-items/${id}`)
            .then(function (response) {
                if(self.preFillForm){
                    self.preFillForm(response.data.data);
                }
            })
            .catch((error) => error);
    }
    updateItem(formData) {
        let self = this.vm;
        let loader = self.$loading.show({
            container: self.$refs.updateMenuItemForm
        });
        axios
        .put(`properties/${self.$store.state.property.id}/menu-items/${formData.id}`, formData)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: 'Item Updated Successfully!', life: 1000 });
                loader.hide();
                self.$router.push({name : 'menu-items'});
            })
            .catch(function (error) {
                loader.hide();
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            self.$toast.add({ severity: 'error', summary: 'Error', detail: errorDetail, life: 3000 });
                        }
                    }
                } else {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    deleteItem(id) {
        let self = this.vm;
        let loader = self.$loading.show();
        axios
        .delete(`properties/${self.$store.state.property.id}/menu-items/${id}`)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: response.data.message, life: 1000 });
                loader.hide();
                self.menuItemService.getItems();
            })
            .catch(function (error) {
                loader.hide();
                if(error.response){
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    getItemsForSale(property) {
        let self = this.vm;
        return axios
            .get(`properties/${property}/menu-items-sales`)
            .then(function (response) {
                self.items = response.data.data;
            })
            .catch((error) => error);
    }
    import(id) {
        let self = this.vm;
        let loader = self.$loading.show();
        return axios
            .post(`properties/${self.$store.state.property.id}/import-menu-items/${id}`)
            .then(function (response) {
                self.menuItemService.getItems();
                self.importProperty = null;
                self.importItemDialog = false;
                self.$toast.add({ severity: 'success', summary: 'Success', detail: response.data.message, life: 1000 });
                loader.hide();
            })
            .catch(function (error) {
                loader.hide();
                if(error.response){
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
}
