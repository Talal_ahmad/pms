export default class MainDashboardService {
    constructor(vm) {
        this.vm = vm;
    }
    index() {
        let self = this.vm;
        let loader = self.$loading.show();
        return self.$axios
            .get('properties-for-dashboard')
            .then(function (response) {
                self.properties = response.data.data;
                loader.hide();
            })
            .catch((error) => error);
    }
}
