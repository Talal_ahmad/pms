export default class MenuServingService {
    constructor(vm) {
        this.vm = vm;
    }
    getServings() {
        let self = this.vm;
        let loader = self.$loading.show();
        return self.$axios
            .get(`properties/${self.$store.state.property.id}/menu-servings`)
            .then(function (response) {
                self.servings = response.data.data;
                if(self.menuServings){
                    self.menuServings = response.data.data.filter(item => item.is_quantity == 0);
                }
                if(self.menuServingQuantities){
                    self.menuServingQuantities = response.data.data.filter(item => item.is_quantity == 1);
                }
                loader.hide();
            })
            .catch((error) => error);
    }
    storeServing(formData) {
        let self = this.vm;
        let loader = self.$loading.show({
            container: self.$refs.addMenuServingForm
        });
        self.$axios
        .post(`properties/${self.$store.state.property.id}/menu-servings`, formData)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: 'Serving Created Successfully!', life: 1000 });
                loader.hide();
                self.$router.push({name : 'menu-servings'});
            })
            .catch(function (error) {
                loader.hide();
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            self.$toast.add({ severity: 'error', summary: 'Error', detail: errorDetail, life: 3000 });
                        }
                    }
                } else {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    getServing(id) {
        let self = this.vm;
        return this.vm.$axios
            .get(`properties/${self.$store.state.property.id}/menu-servings/${id}`)
            .then(function (response) {
                self.formData = response.data.data;
            })
            .catch((error) => error);
    }
    updateServing(formData) {
        let self = this.vm;
        let loader = self.$loading.show({
            container: self.$refs.updateMenuServingForm
        });
        self.$axios
        .put(`properties/${self.$store.state.property.id}/menu-servings/${formData.id}`, formData)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: 'Serving Updated Successfully!', life: 1000 });
                loader.hide();
                self.$router.push({name : 'menu-servings'});
            })
            .catch(function (error) {
                loader.hide();
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            self.$toast.add({ severity: 'error', summary: 'Error', detail: errorDetail, life: 3000 });
                        }
                    }
                } else {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    deleteServing(id) {
        let self = this.vm;
        let loader = self.$loading.show();
        self.$axios
        .delete(`properties/${self.$store.state.property.id}/menu-servings/${id}`)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: response.data.message, life: 1000 });
                loader.hide();
                self.menuServingService.getServings();
            })
            .catch(function (error) {
                loader.hide();
                if(error.response){
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    import(id) {
        let self = this.vm;
        let loader = self.$loading.show();
        return self.$axios
            .post(`properties/${self.$store.state.property.id}/import-menu-servings/${id}`)
            .then(function (response) {
                self.menuServingService.getServings();
                self.importProperty = null;
                self.importServingDialog = false;
                self.$toast.add({ severity: 'success', summary: 'Success', detail: response.data.message, life: 1000 });
                loader.hide();
            })
            .catch(function (error) {
                loader.hide();
                if(error.response){
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
}
