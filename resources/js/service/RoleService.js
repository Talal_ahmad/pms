export default class RoleService {
    constructor(vm) {
        this.vm = vm;
    }
    getRoles() {
        let self = this.vm;
        let loader = self.$loading.show();
        return self.$axios
            .get('roles')
            .then(function (response) {
                self.roles = response.data.data;
                loader.hide();
            })
            .catch((error) => error);
    }
    storeRole(formData) {
        let self = this.vm;
        let loader = self.$loading.show({
            container: self.$refs.addRoleForm
        });
        self.$axios
            .post('/roles', formData)
            .then(function (response) {
                self.roleAddDialog = false;
                self.$toast.add({ severity: 'success', summary: 'Success', detail: 'Role Created Successfully!', life: 1000 });
                loader.hide();
                self.roleService.getRoles();
            })
            .catch(function (error) {
                loader.hide();
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            self.$toast.add({ severity: 'error', summary: 'Error', detail: errorDetail, life: 3000 });
                        }
                    }
                } else {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    getRole(id) {
        let self = this.vm;
        return self.$axios
            .get('roles' + '/' + id)
            .then(function (response) {
                self.formData.role = response.data.data;
                if (self.role) {
                    self.role = response.data.data;
                }
            })
            .catch((error) => error);
    }
    getPermissionByRole(id) {
        let self = this.vm;
        return self.$axios
            .get('permissions-by-role' + '/' + id)
            .then(function (response) {
                self.formData.permissions = response.data.data.map((permission) => permission.id);
            })
            .catch((error) => error);
    }
    updateRole(formData) {
        let self = this.vm;
        let loader = self.$loading.show({
            container: self.$refs.updateRoleForm
        });
        self.$axios
            .put('/roles' + '/' + formData.id, formData)
            .then(function (response) {
                self.roleEditDialog = false;
                self.$toast.add({ severity: 'success', summary: 'Success', detail: 'Role Updated Successfully!', life: 1000 });
                loader.hide();
                self.roleService.getRoles();
            })
            .catch(function (error) {
                loader.hide();
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            self.$toast.add({ severity: 'error', summary: 'Error', detail: errorDetail, life: 3000 });
                        }
                    }
                } else {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    deleteRole(id) {
        let self = this.vm;
        let loader = self.$loading.show();
        self.$axios
            .delete('/roles' + '/' + id)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: response.data.message, life: 1000 });
                loader.hide();
                self.roleService.getRoles();
            })
            .catch(function (error) {
                loader.hide();
                if (error.response) {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    syncPermissions(formData) {
        let self = this.vm;
        let loader = self.$loading.show();
        self.$axios
            .put('/sync-role-permissions' + '/' + formData.role.id, formData)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: response.data.message, life: 1000 });
                loader.hide();
                self.$router.push('/roles');
            })
            .catch(function (error) {
                loader.hide();
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            self.$toast.add({ severity: 'error', summary: 'Error', detail: errorDetail, life: 3000 });
                        }
                    }
                } else {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
}
