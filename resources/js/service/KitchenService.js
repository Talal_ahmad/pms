import axios from '../axios';

export default class KitchenService {
    constructor(vm) {
        this.vm = vm;
    }
    getOrders(property) {
        let self = this.vm;
        return axios
            .get(`properties/${property}/kitchen-orders`)
            .then(function (response) {
                self.orders = response.data.data;
                loader.hide();
            })
            .catch((error) => error);
    }
    updateOrder(id,property) {
        let self = this.vm;
        let loader = self.$loading.show();
        self.$axios
        .put(`properties/${property}/mark-order-ready/${id}`)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: 'Order Updated Successfully!', life: 1000 });
                loader.hide();
                self.kitchenService.getOrders();
            })
            .catch(function (error) {
                loader.hide();
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            self.$toast.add({ severity: 'error', summary: 'Error', detail: errorDetail, life: 3000 });
                        }
                    }
                } else {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
}
