import axios from '../axios';
import {useToast} from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
const $toast = useToast();
export default class OrderService {
    constructor(vm) {
        this.vm = vm;
    }
    getOrders(dateFilter = '') {
        let self = this.vm;
        let loader = self.$loading.show();
        return self.$axios
            .get(`properties/${self.$store.state.property.id}/orders`, {
                        params: {
                            dateFilter: dateFilter,
                        },
                    })
            .then(function (response) {
                self.sales = response.data.data;
                loader.hide();
            })
            .catch((error) => error);
    }
    storeOrder(property, formData) {
        let self = this.vm;
        axios
        .post(`properties/${property}/orders`, formData)
            .then(function (response) {
                $toast.open({
                    message: 'Order Created Successfully!',
                    type: 'success',
                    position: 'top-right'
                });
                self.printOrderData = response.data.data;
                self.showOrderSuccessDialog = true;
                self.clearForm();
            })
            .catch(function (error) {
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            $toast.open({
                                message: errorDetail,
                                type: 'error',
                                position: 'top-right'
                            });
                        }
                    }
                } else {
                    $toast.open({
                        message: error.response.data.message,
                        type: 'error',
                        position: 'top-right'
                    });
                }
            });
    }
    getOrder(property, id) {
        let self = this.vm;
        return axios
            .get(`properties/${property}/orders/${id}`)
            .then(function (response) {
                self.formData = response.data.data;
                if(self.preFillForm){
                    self.preFillForm(response.data.data);
                }
            })
            .catch((error) => error);
    }
    updateOrder(property, formData) {
        let self = this.vm;
        axios
        .put(`properties/${property}/orders/${formData.id}`, formData)
            .then(function (response) {
                $toast.open({
                    message: 'Order Updated Successfully!',
                    type: 'success',
                    position: 'top-right'
                });
                self.printOrderData = response.data.data;
                self.showOrderSuccessDialog = true;
            })
            .catch(function (error) {
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            $toast.open({
                                message: errorDetail,
                                type: 'error',
                                position: 'top-right'
                            });
                        }
                    }
                } else {
                    $toast.open({
                        message: error.response.data.message,
                        type: 'error',
                        position: 'top-right'
                    });
                }
            });
    }
    deleteOrder(id) {
        let self = this.vm;
        let loader = self.$loading.show();
        self.$axios
        .delete(`properties/${self.$store.state.property.id}/orders/${id}`)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: response.data.message, life: 1000 });
                loader.hide();
                self.orderService.getOrders();
            })
            .catch(function (error) {
                loader.hide();
                if(error.response){
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    getTables(){
        let self = this.vm;
        return this.vm.$axios
            .get(`properties/${self.$store.state.property.id}/tables-for-making-orders`)
            .then(function (response) {
                self.tableOptions = response.data.data;
            })
            .catch((error) => error);
    }
}
