export default class FloorService {
    constructor(vm) {
        this.vm = vm;
    }
    getFloors() {
        let self = this.vm;
        let loader = self.$loading.show();
        return self.$axios
            .get(`properties/${self.$store.state.property.id}/floors`)
            .then(function (response) {
                self.floors = response.data.data;
                loader.hide();
            })
            .catch((error) => error);
    }
    storeFloors(formData) {
        let self = this.vm;
        let loader = self.$loading.show({
            container: self.$refs.FloorsForm
        });
        self.$axios
        .post(`properties/${self.$store.state.property.id}/floors`, formData)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: 'Floors Created Successfully!', life: 1000 });
                loader.hide();
                self.$router.push({name : 'floors'});
            })
            .catch(function (error) {
                loader.hide();
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            self.$toast.add({ severity: 'error', summary: 'Error', detail: errorDetail, life: 3000 });
                        }
                    }
                } else {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    getFloor(id) {
        let self = this.vm;
        return this.vm.$axios
            .get(`properties/${self.$store.state.property.id}/floors/${id}`)
            .then(function (response) {
                self.formData = response.data.data;
            })
            .catch((error) => error);
    }
    updateFloor(formData) {
        let self = this.vm;
        let loader = self.$loading.show({
            container: self.$refs.floorForm
        });
        self.$axios
        .put(`properties/${self.$store.state.property.id}/floors/${formData.id}`, formData)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: 'Floor Updated Successfully!', life: 1000 });
                loader.hide();
                self.$router.push({name : 'floors'});
            })
            .catch(function (error) {
                loader.hide();
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            self.$toast.add({ severity: 'error', summary: 'Error', detail: errorDetail, life: 3000 });
                        }
                    }
                } else {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    deleteFloor(id) {
        let self = this.vm;
        let loader = self.$loading.show();
        self.$axios
        .delete(`properties/${self.$store.state.property.id}/floors/${id}`)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: response.data.message, life: 1000 });
                loader.hide();
                self.floorService.getFloors();
            })
            .catch(function (error) {
                loader.hide();
                if(error.response){
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
}
