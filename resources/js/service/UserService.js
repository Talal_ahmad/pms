export default class UserService {
    constructor(vm) {
        this.vm = vm;
    }
    getUsers() {
        let self = this.vm;
        let loader = self.$loading.show();
        return self.$axios
            .get('users')
            .then(function (response) {
                self.users = response.data.data;
                loader.hide();
            })
            .catch((error) => error);
    }
    storeUser(formData) {
        let self = this.vm;
        let loader = self.$loading.show({
            container: self.$refs.userForm
        });
        self.$axios
            .post('/users', formData)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: 'User Created Successfully!', life: 1000 });
                loader.hide();
                self.$router.push('/users');
            })
            .catch(function (error) {
                loader.hide();
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            self.$toast.add({ severity: 'error', summary: 'Error', detail: errorDetail, life: 3000 });
                        }
                    }
                } else {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    getUser(id) {
        let self = this.vm;
        return this.vm.$axios
            .get('users' + '/' + id)
            .then(function (response) {
                self.formData = response.data.data;
                self.formData.properties = response.data.data.properties.map(property => property.id);
                self.formData.role = response.data.data.roles[0]?.id;
            })
            .catch((error) => error);
    }
    updateUser(formData) {
        let self = this.vm;
        let loader = self.$loading.show({
            container: self.$refs.userForm
        });
        self.$axios
            .put('/users' + '/' + formData.id, formData)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: 'User Updated Successfully!', life: 1000 });
                loader.hide();
                self.$router.push('/users');
            })
            .catch(function (error) {
                loader.hide();
                if (error.response && error.response.data && error.response.data.errors) {
                    for (const fieldErrors of Object.values(error.response.data.errors)) {
                        for (const errorDetail of fieldErrors) {
                            self.$toast.add({ severity: 'error', summary: 'Error', detail: errorDetail, life: 3000 });
                        }
                    }
                } else {
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
    deleteUser(id) {
        let self = this.vm;
        let loader = self.$loading.show();
        self.$axios
            .delete('/users' + '/' + id)
            .then(function (response) {
                self.$toast.add({ severity: 'success', summary: 'Success', detail: response.data.message, life: 1000 });
                loader.hide();
                self.userService.getUsers();
            })
            .catch(function (error) {
                loader.hide();
                if(error.response){
                    self.$toast.add({ severity: 'error', summary: 'Error', detail: error.response.data.message, life: 3000 });
                }
            });
    }
}
