import { createApp } from 'vue';
import PosCreateComponent from './Components/Pos/CreateComponent.vue';
import PosEditComponent from './Components/Pos/EditComponent.vue';
import KitchenComponent from './Components/Kitchen/Index.vue';

const app = createApp({});

app.component('PosCreateComponent', PosCreateComponent);
app.component('PosEditComponent', PosEditComponent);
app.component('KitchenComponent',KitchenComponent);

app.mount("#app");
